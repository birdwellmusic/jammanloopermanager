﻿using System;
using System.Globalization;
using JamManClassicLib;

namespace JamManCommand
{
    public class Program
    {
        /// <summary>
        /// JamManCommand is a command-line interface used for testing of core features. 
        /// Every action will generate a snapshot "cache" of html and data that can be reviewed.
        /// It's entirely possible to use this commandline version as your JamMan librarian
        /// however that's a slow process and this command-line version was intended as a bridge tool
        /// until a .NET Core web interface could be built and this was the foundation!
        /// </summary>
        /// <param name="args"></param>
        static void Main(string[] args)
        {
            // TODO: additional command functionality:
            // 1. DONE: Library Item Move Up and Down
            // 2. DONE: Slot Item Move Up and Down
            // 3. FUTURE: Import Multiple Files into Library (append) - not really needed? -RB

            var jmNotifier = new JMNotifier();

            jmNotifier.CommandInfo += JmNotifier_CommandInfo;
            jmNotifier.Status += JmNotifier_Status;

            if ( args == null || args.Length == 0)
            {
                JMCommands.ShowHelp(jmNotifier);
                return;
            }

            var command = args[0];

            switch (command)
            {
                case "-initAll":
                {
                    JMCommands.InitAll(jmNotifier);
                    break;
                }

                case "-cacheStatus":
                {
                    JMCommands.CacheStatus(jmNotifier);
                    break;
                }

                case "-cacheInit":
                {
                    JMCommands.CacheInit(jmNotifier);
                    break;
                }

                case "-cacheUpdate":
                {
                    JMCommands.CacheUpdate(jmNotifier);
                    break;
                }

                case "-settingsInit":
                {
                    JMCommands.SettingsInit(jmNotifier);
                    break;
                }

                case "-libraryInit":
                {
                    JMCommands.LibraryInit(jmNotifier);
                    break;
                }

                case "-libraryAdd":
                {
                    JMCommands.LibraryAdd(args, jmNotifier);
                    break;
                }

                case "-libraryRemove":
                {
                    JMCommands.LibraryRemove(args, jmNotifier);
                    break;
                }

                case "-libraryArchive":
                {
                    JMCommands.LibraryArchive(args, jmNotifier);
                    break;
                }

                case "-libraryUpdate":
                {
                    JMCommands.LibraryUpdate(args, jmNotifier);
                    break;
                }

                case "-libraryReport":
                {
                    JMCommands.LibraryReport(jmNotifier);
                    break;
                }

                case "-libraryMoveItemUp":
                {
                    JMCommands.LibraryMoveItemUp(args, jmNotifier);
                    break;
                }

                case "-libraryMoveItemDown":
                {
                    JMCommands.LibraryMoveItemDown(args, jmNotifier);
                    break;
                }

                case "-jammanInit":
                {
                    JMCommands.JammanInit(jmNotifier);
                    break;
                }

                case "-jammanAssignSlot":
                {
                    JMCommands.JammanAssignSlot(args, jmNotifier);
                    break;
                }

                case "-jammanClearSlot":
                {
                    JMCommands.JammanClearSlot(args, jmNotifier);
                    break;
                }

                case "-jammanMoveSlotUp":
                {
                    JMCommands.JammanMoveSlotUp(args, jmNotifier);
                    break;
                }

                case "-jammanMoveSlotDown":
                {
                    JMCommands.JammanMoveSlotDown(args, jmNotifier);
                    break;
                }

                case "-jammanClearAllSlots":
                {
                    JMCommands.JammanClearAllSlots(jmNotifier);
                    break;
                }

                case "-jammanSaveAll":
                {
                    JMCommands.JammanSaveAll(jmNotifier);
                    break;
                }

                case "-jammanSyncToLibrary": 
                {
                    JMCommands.JammanSyncToLibrary(jmNotifier);
                    break;
                }

                case "-jammanReport":
                {
                    JMCommands.JamManReport(jmNotifier);
                    break;
                }

                case "-help":
                {
                    JMCommands.ShowHelp(jmNotifier);
                    break;
                }

                default: // unknown command
                {
                    JMCommands.UnknownCommand(jmNotifier);
                    JMCommands.ShowHelp(jmNotifier);
                    break;
                }
            }
        }

        private static void JmNotifier_Status(string message, double percentDone)
        {
            if (string.IsNullOrWhiteSpace(message) == false && percentDone > 0.0)
            {
                Console.WriteLine("{0} - {1}", percentDone.ToString("P", CultureInfo.InvariantCulture), message);
            }
        }

        private static void JmNotifier_CommandInfo(CommandType commandType, CommandState commandState)
        {
            Console.WriteLine("COMMAND TYPE: {0} COMMAND STATE: {1}", commandType.ToString("g"), commandState.ToString("g"));

            if ( commandType == CommandType.ShowHelp)
            {
                Console.WriteLine("SYNTAX: dotnet jm.dll [-command] [arg1 argN ...]");
                Console.WriteLine("* * * * *");
                Console.WriteLine("See Program.cs in the JamManCommand project source code for the full list of commands.");
            }
        }
    }
}