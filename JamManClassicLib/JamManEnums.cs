﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace JamManClassicLib
{
    /// <summary>
    /// Valid values must be string format
    /// </summary>
    public enum LoopMode
    {
        Loop,
        Single
    }

    /// <summary>
    /// Valid values are 0 through 13
    /// </summary>
    public enum TimeSignature
    {
        TwoFour,
        ThreeFour,
        FourFour,
        FiveFour,
        SixFour,
        SevenFour,
        EightFour,
        NineFour,
        TenFour,
        ElevenFour,
        TwelveFour,
        ThirteenFour,
        FourteenFour,
        FifteenFour
    }

    /// <summary>
    /// Valid values are 0 through 9
    /// </summary>
    public enum Rhythm
    {
        None,
        WoodBlocks,
        Sticks,
        Click,
        AlternateKickHighHat,
        StudioKickHighHat,
        TechnoKickHighHat,
        CowBell,
        Conga,
        Tambourine
    }

    /// <summary>
    /// Valid values must be string format
    /// </summary>
    public enum StopMode
    {
        Stop,
        Finish
    }

    public enum CommandState
    {
        OneShot,
        Started,
        Completed
    }

    public enum CommandType
    {
        InitAll = 100,
        
        CacheInit = 200,
        CacheUpdate = 300,
        CacheUpdateJSON = 310,
        CacheStatus = 400,
        CacheGenerateHtmlFileIfNotExist = 410,
        SettingsInit = 500,

        LibraryInit = 600,
        LibraryAdd = 700,
        LibraryRemove = 800,

        LibraryMoveItemUp = 810,
        LibraryMoveItemDown = 820,

        LibraryArchive = 900,
        LibraryUpdate = 1000,
        LibraryReport = 1100,


        JammanInit = 1200,
        JammanAssignSlot = 1300,
        JammanClearSlot = 1400,
        JammanClearAllSlots = 1500,
        
        JammanMoveSlotUp = 1510,
        JammanMoveSlotDown = 1520,
        
        JammanSaveAll = 1600,
        JammanSyncToLibrary = 1700,
        JammanReport = 1710,

        ShowHelp = 1800,


        UnknownCommand = 5000
    }

}
