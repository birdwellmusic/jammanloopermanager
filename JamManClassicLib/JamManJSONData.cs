﻿using System;

namespace JamManClassicLib
{
    public class JamManJSONData
    {
        public JMCLibrary Library;
        public JamManConnectInfo ConnectInfo;
        public JMCSettings Settings;
        public DateTime UpdateDate;

        public JamManJSONData(JMCLibrary library, JamManConnectInfo connectInfo, JMCSettings settings)
        {
            Library = library;
            ConnectInfo = connectInfo;
            Settings = settings;
            UpdateDate = DateTime.Now;
        }
    }
}
