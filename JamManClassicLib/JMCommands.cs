﻿using Newtonsoft.Json;
using System;
using System.IO;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace JamManClassicLib
{
    public static class JMCommands
    {
        private static readonly string mJmCacheDir = GetAppPath() + "\\JMCache";
        private static readonly string mJmUploadsDir = GetAppPath() + "\\JMUploads";
        private static readonly string mIndexHtmlFile = mJmCacheDir + "\\index.html";
        private static readonly string mJmConnectionInfoFile = mJmCacheDir + "\\JMConnectionInfo.xml";
        private static readonly string mJmSettingsFile = mJmCacheDir + "\\JMSettings.xml";
        private static readonly string mJmJson = mJmCacheDir + "\\JM.json";
        private static readonly string mJmLibDir = GetAppPath() + "\\JMLibrary";
        private static readonly string mJmLibFile = mJmLibDir + "\\JMLibrary.xml";
        private static readonly string mJmLibReportFile = mJmCacheDir + "\\JMLibraryReport.html"; // writes to the cache
        private static readonly string mJamManDir = GetAppPath() + "\\JAMMAN";
        private static readonly string mJamManJMCSyncFile = mJamManDir + "\\JMSync.xml";
        private static readonly string mJamManSetupFile = mJamManDir + "\\SETUP.XML";
        private static readonly string mJamManReportFile = mJmCacheDir + "\\JMReport.html"; // writes to the cache

        private static readonly string mJmArchiveDir = GetAppPath() + "\\JMArchive";

        public static string LibraryDirectory
        {
            get { return mJmLibDir; }
        }

        public static string JamManDirectory
        {
            get { return mJamManDir; }
        }

        public static string ArchiveDirectory
        {
            get { return mJmArchiveDir; }
        }

        public static string LibraryReportFile
        {
            get { return mJmLibReportFile; }
        }

        public static string JamManReportFile
        {
            get { return mJamManReportFile; }
        }

        public static void InitAll(JMNotifier notify)
        {
            notify?.CommandInfo(CommandType.InitAll, CommandState.Started);

            CacheInit(notify);
            SettingsInit(notify);
            LibraryInit(notify);
            JammanInit(notify);
            CacheUpdate(notify);

            notify?.CommandInfo(CommandType.InitAll, CommandState.Completed);
        }

        public static void JamManReport(JMNotifier notify)
        {
            notify?.CommandInfo(CommandType.JammanReport, CommandState.Started);

            JMCLibrary library = new JMCLibrary();
            if (library.Load(mJmLibFile) == false)
            {
                notify?.Status("JammanReport: FAILED. Unable to load existing library. -initAll may need to be called.", 1);
                return;
            }

            var connectionInfo = new JamManConnectInfo();
            if (connectionInfo.Load(mJmConnectionInfoFile, mJamManDir, library) == false)
            {
                notify?.Status("JammanReport: FAILED.  Unable to load connection info from file: " + mJmConnectionInfoFile, 1);
                return;
            }

            if (library.GenerateJamManReport(library, connectionInfo, mJamManReportFile) == false)
            {
                notify?.Status("JammanReport: FAILED. Unable to generate the JAMMAN set report file: " + mJamManReportFile, 1);
                return;
            }

            notify?.Status("JammanReport: SUCCESS.", 1);
            notify?.CommandInfo(CommandType.JammanReport, CommandState.Completed);
        }

        public static void JammanInit(JMNotifier notify)
        {
            notify?.CommandInfo(CommandType.JammanInit, CommandState.Started);

            if (Directory.Exists(mJamManDir) == false)
            {
                try
                {
                    Directory.CreateDirectory(mJamManDir);
                }
                catch
                {
                    notify?.Status("JammanInit: FAILED.  Unable to create directory: " + mJamManDir, 1);
                    return;
                }
            }

            JMCLibrary library = new JMCLibrary();
            if (library.Load(mJmLibFile) == false)
            {
                notify?.Status("JammanInit: FAILED. Unable to load existing library.", 1);
                return;
            }

            // if the connection info file doesn't exist, create one...
            if (File.Exists(mJmConnectionInfoFile) == false)
            {
                var jmConnectionInfo = new JamManConnectInfo();
                jmConnectionInfo.ConnectionName = "JAMMAN";
                jmConnectionInfo.SetJamManFullPath(mJamManDir);

                if (jmConnectionInfo.Save(mJmConnectionInfoFile, library) == false)
                {
                    notify?.Status("JammanInit: FAILED.  Unable to create connection info file: " + mJmConnectionInfoFile, 1);
                    return;
                }
            }

            var connectionInfo = new JamManConnectInfo();
            if (connectionInfo.Load(mJmConnectionInfoFile, mJamManDir, library) == false)
            {
                notify?.Status("JammanInit: FAILED.  Unable to load connection info from file: " + mJmConnectionInfoFile, 1);
                return;
            }

            if (connectionInfo.SaveSetupFileOnDevice() == false)
            {
                notify?.Status("JammanInit: FAILED.  Unable to save setup file for internal device: " + mJamManSetupFile, 1);
                return;
            }

            var errorMsg = "";
            if (library.SaveJamManSyncFile(connectionInfo, out errorMsg) == false)
            {
                notify?.Status("JammanInit: FAILED.  Unable to save sync file for internal device: " + mJamManJMCSyncFile, 1);
                return;
            }

            notify?.Status("JammanInit: SUCCESS.", 1);

            notify?.CommandInfo(CommandType.JammanInit, CommandState.Completed);
        }

        public static void JammanSyncToLibrary(JMNotifier notify)
        {
            notify?.CommandInfo(CommandType.JammanSyncToLibrary, CommandState.Started);

            JMCLibrary library = new JMCLibrary();
            if (library.Load(mJmLibFile) == false)
            {
                notify?.Status("JammanSyncToLibrary: FAILED. Unable to load existing library. -initAll may need to be called.", 1);
                return;
            }

            var connectionInfo = new JamManConnectInfo();
            if (connectionInfo.Load(mJmConnectionInfoFile, mJamManDir, library) == false)
            {
                notify?.Status("JammanSyncToLibrary: FAILED.  Unable to load connection info from file: " + mJmConnectionInfoFile, 1);
                return;
            }

            var settings = new JMCSettings();
            if (settings.Load(mJmSettingsFile) == false)
            {
                notify?.Status("JammanSyncToLibrary: FAILED.  Unable to load settings file: " + mJmSettingsFile, 1);
                return;
            }

            if (library.LoadFromJMCSync(connectionInfo, settings, notify) == false)
            {
                notify?.Status("JammanSyncToLibrary: FAILED.  LoadFromJMCSync was unable to load/sync from device.", 1);
                return;
            }

            // since library items could have been added from the sync we save the library now...
            if (library.SaveLibrary(mJmLibFile, false) == false)
            {
                notify?.Status("JammanSyncToLibrary: FAILED. SaveLibrary failed. Target file may be LOCKED or this tool does not have WRITE permissions: " + mJmLibFile, 1);
                return;
            }

            // This would have happened already if LoadFromJMCSync was successful....
            //// re-sync to this local jamman to reflect current state...
            //JammanSaveAll(showStatus);

            notify?.Status("JammanSyncToLibrary: SUCCESS.", 1);

            // Update the cache...
            CacheUpdate(notify);

            notify?.CommandInfo(CommandType.JammanSyncToLibrary, CommandState.Completed);
        }

        public static void JammanSaveAll(JMNotifier notify)
        {
            // We have the params...gather the library and connection info and clear slot...

            notify?.CommandInfo(CommandType.JammanSaveAll, CommandState.Started);

            JMCLibrary library = new JMCLibrary();
            if (library.Load(mJmLibFile) == false)
            {
                notify?.Status("JammanSaveAll: FAILED. Unable to load existing library. -initAll may need to be called.", 1);
                return;
            }

            var connectionInfo = new JamManConnectInfo();
            if (connectionInfo.Load(mJmConnectionInfoFile, mJamManDir, library) == false)
            {
                notify?.Status("JammanSaveAll: FAILED.  Unable to load connection info from file: " + mJmConnectionInfoFile, 1);
                return;
            }

            var errorMessage = "";
            if (library.SaveAllToJamMan(connectionInfo, out errorMessage, notify?.Status) == false)
            {
                notify?.Status("JammanSaveAll: FAILED.  Error details: " + errorMessage, 1);
                return;
            }

            notify?.Status("JammanSaveAll: SUCCESS.", 1);

            // Update the cache...
            CacheUpdate(notify);

            notify?.CommandInfo(CommandType.JammanSaveAll, CommandState.Completed);
        }

        public static void JammanClearAllSlots(JMNotifier notify)
        {
            // jm - jammanClearAllSlots

            notify?.CommandInfo(CommandType.JammanClearAllSlots, CommandState.Started);

            JMCLibrary library = new JMCLibrary();
            if (library.Load(mJmLibFile) == false)
            {
                notify?.Status("JammanClearAllSlots: FAILED. Unable to load existing library. -initAll may need to be called.", 1);
                return;
            }

            var connectionInfo = new JamManConnectInfo();
            if (connectionInfo.Load(mJmConnectionInfoFile, mJamManDir, library) == false)
            {
                notify?.Status("JammanClearAllSlots: FAILED.  Unable to load connection info from file: " + mJmConnectionInfoFile, 1);
                return;
            }

            // This should do it...note that no library id is being saved which should be okay...
            connectionInfo.InitEmptySlots();

            // Save the connection info with this slot assignment!
            if (connectionInfo.Save(mJmConnectionInfoFile, library) == false)
            {
                notify?.Status("JammanClearAllSlots: FAILED.  Unable to save connection info to clear slots.", 1);
                return;
            }

            notify?.Status("JammanClearAllSlots: SUCCESS.", 1);

            // Update the cache...
            CacheUpdate(notify);

            notify?.CommandInfo(CommandType.JammanClearAllSlots, CommandState.Completed);
        }

        public static void JammanMoveSlotUp(string[] args, JMNotifier notify)
        {
            //    args[0]          args[1]  
            // jm -jammanMoveSlotUp [SLOTINDEX]

            notify?.CommandInfo(CommandType.JammanMoveSlotUp, CommandState.Started);

            if (args == null || args.Length != 2)
            {
                notify?.Status("SYNTAX for -jammanMoveSlotUp tip: must have the exact parameter of a [SLOTINDEX]. Indicies are 1-based.", 1);
                return;
            }

            var oneBasedIndex = -1;
            if (int.TryParse(args[1], out oneBasedIndex) == false)
            {
                notify?.Status("[SLOTINDEX] param must not be empty and must be valid.", 1);
                return;
            }

            JMCLibrary library = new JMCLibrary();
            if (library.Load(mJmLibFile) == false)
            {
                notify?.Status("JammanMoveSlotUp: FAILED. Unable to load existing library. -initAll may need to be called.", 1);
                return;
            }

            var connectionInfo = new JamManConnectInfo();
            if (connectionInfo.Load(mJmConnectionInfoFile, mJamManDir, library) == false)
            {
                notify?.Status("JammanMoveSlotUp: FAILED.  Unable to load connection info from file: " + mJmConnectionInfoFile, 1);
                return;
            }

            if (connectionInfo.SwapUp(oneBasedIndex - 1))
            {
                // Save the connection info with this slot assignment!
                if (connectionInfo.Save(mJmConnectionInfoFile, library) == false)
                {
                    notify?.Status("JammanMoveSlotUp: FAILED.  Unable to save connection info to clear slot.", 1);
                    return;
                }

                notify?.Status("JammanMoveSlotUp: SUCCESS.", 1);

                // Update the cache...
                CacheUpdate(notify);
            }
            else
            {
                notify?.Status("JammanMoveSlotUp: NO ACTION. (Nothing was swapped/moved since the index was out of range.)", 1);
            }

            notify?.CommandInfo(CommandType.JammanMoveSlotUp, CommandState.Completed);
        }

        public static void JammanMoveSlotDown(string[] args, JMNotifier notify)
        {
            //    args[0]             args[1]  
            // jm -jammanMoveSlotDown [SLOTINDEX]

            notify?.CommandInfo(CommandType.JammanMoveSlotDown, CommandState.Started);

            if (args == null || args.Length != 2)
            {
                notify?.Status("SYNTAX for -jammanMoveSlotDown tip: must have the exact parameter of a [SLOTINDEX]. Indicies are 1-based.", 1);
                return;
            }

            var oneBasedIndex = -1;
            if (int.TryParse(args[1], out oneBasedIndex) == false)
            {
                notify?.Status("[SLOTINDEX] param must not be empty and must be valid.", 1);
                return;
            }

            JMCLibrary library = new JMCLibrary();
            if (library.Load(mJmLibFile) == false)
            {
                notify?.Status("JammanMoveSlotDown: FAILED. Unable to load existing library. -initAll may need to be called.", 1);
                return;
            }

            var connectionInfo = new JamManConnectInfo();
            if (connectionInfo.Load(mJmConnectionInfoFile, mJamManDir, library) == false)
            {
                notify?.Status("JammanMoveSlotDown: FAILED.  Unable to load connection info from file: " + mJmConnectionInfoFile, 1);
                return;
            }

            if (connectionInfo.SwapDown(oneBasedIndex - 1))
            {
                // Save the connection info with this slot assignment!
                if (connectionInfo.Save(mJmConnectionInfoFile, library) == false)
                {
                    notify?.Status("JammanMoveSlotDown: FAILED.  Unable to save connection info to clear slot.", 1);
                    return;
                }

                notify?.Status("JammanMoveSlotDown: SUCCESS.", 1);

                // Update the cache...
                CacheUpdate(notify);
            }
            else
            {
                notify?.Status("JammanMoveSlotDown: NO ACTION. (Nothing was swapped/moved since the index was out of range.)", 1);
            }

            notify?.CommandInfo(CommandType.JammanMoveSlotDown, CommandState.Completed);
        }

        public static void JammanClearSlot(string[] args, JMNotifier notify)
        {
            //    args[0]          args[1]  
            // jm -jammanClearSlot [SLOTINDEX]

            notify?.CommandInfo(CommandType.JammanClearSlot, CommandState.Started);

            if (args == null || args.Length != 2)
            {
                ShowJammanClearSlotSyntax("Invalid arguguments.", notify);
                return;
            }

            var slotIndex = -1;
            if (int.TryParse(args[1], out slotIndex) == false)
            {
                ShowJammanClearSlotSyntax("[SLOTINDEX] param must not be empty and must be valid.", notify);
                return;
            }

            // We have the params...gather the library and connection info and clear slot...

            JMCLibrary library = new JMCLibrary();
            if (library.Load(mJmLibFile) == false)
            {
                notify?.Status("JammanClearSlot: FAILED. Unable to load existing library. -initAll may need to be called.", 1);
                return;
            }

            var connectionInfo = new JamManConnectInfo();
            if (connectionInfo.Load(mJmConnectionInfoFile, mJamManDir, library) == false)
            {
                notify?.Status("JammanClearSlot: FAILED.  Unable to load connection info from file: " + mJmConnectionInfoFile, 1);
                return;
            }

            // We should be able to assign the slot now...note we're not transfering or finalizing
            // the jamaman connection...just a slot assign.

            var slotInfo = new JamManSlotInfo();

            slotInfo.LibItemId = string.Empty;
            slotInfo.LibItemName = string.Empty;
            slotInfo.SlotNumber = slotIndex;
            slotInfo.IsEmpty = true;
            slotInfo.LoopInfo = new JamManLoopInfo();

            connectionInfo.Slots[slotIndex - 1] = slotInfo;

            // Save the connection info with this slot assignment!
            if (connectionInfo.Save(mJmConnectionInfoFile, library) == false)
            {
                notify?.Status("JammanClearSlot: FAILED.  Unable to save connection info to clear slot.", 1);
                return;
            }

            notify?.Status("JammanClearSlot: SUCCESS.", 1);

            // Update the cache...
            CacheUpdate(notify);

            notify?.CommandInfo(CommandType.JammanClearSlot, CommandState.Completed);
        }

        public static void JammanAssignSlot(string[] args, JMNotifier notify)
        {
            //    args[0]           args[1]              args[2]  
            // jm -jammanAssignSlot [LIBGUID | LIBINDEX] [SLOTINDEX]

            notify?.CommandInfo(CommandType.JammanAssignSlot, CommandState.Started);

            if (args == null || args.Length != 3)
            {
                ShowJammanAssignSlotSyntax("Invalid arguguments.", notify);
                return;
            }

            var libGuidOrLibIndex = args[1].Replace("\"", "");

            if (string.IsNullOrWhiteSpace(libGuidOrLibIndex))
            {
                ShowJammanAssignSlotSyntax("[LIBGUID | LIBINDEX] param must not be empty.", notify);
                return;
            }

            var libGuid = libGuidOrLibIndex.Length > 30 && libGuidOrLibIndex.Contains("-") ? libGuidOrLibIndex : null;
            var libIndex = -1;

            if (libGuid == null)
            {
                if (int.TryParse(libGuidOrLibIndex, out libIndex) == false)
                {
                    ShowJammanAssignSlotSyntax("[LIBGUID | LIBINDEX] param must not be empty and must be either the guid of the library item or its 1-based index.", notify);
                    return;
                }
            }

            var slotIndex = -1;
            if (int.TryParse(args[2], out slotIndex) == false)
            {
                ShowJammanAssignSlotSyntax("[SLOTINDEX] param must not be empty and must be valid.", notify);
                return;
            }

            // We have the params...gather the library and the connection info...

            JMCLibrary library = new JMCLibrary();
            if (library.Load(mJmLibFile) == false)
            {
                notify?.Status("JammanAssignSlot: FAILED. Unable to load existing library. -initAll may need to be called.", 1);
                return;
            }

            var connectionInfo = new JamManConnectInfo();
            if (connectionInfo.Load(mJmConnectionInfoFile, mJamManDir, library) == false)
            {
                notify?.Status("JammanAssignSlot: FAILED.  Unable to load connection info from file: " + mJmConnectionInfoFile, 1);
                return;
            }

            JMCLibraryItem libItem;

            if (libGuid != null)
            {
                libItem = library.FindItem(libGuid);
                if (libItem == null)
                {
                    notify?.Status("JammanAssignSlot: FAILED.  Unable to find a library item with this guid: " + libGuid, 1);
                    return;
                }
            }
            else // find by index
            {
                if (libIndex < 1 || libIndex > library.LibraryItems.Count)
                {
                    notify?.Status("JammanAssignSlot: FAILED.  The libIndex passed is out of range for the current library: " + libIndex, 1);
                    return;
                }

                libItem = library.LibraryItems[libIndex - 1];
            }

            if (libItem == null)
            {
                notify?.Status("JammanAssignSlot: FAILED.  Unable to fetch a library item with the params.", 1);
                return;
            }

            // We should be able to assign the slot now...note we're not transfering or finalizing
            // the jamaman connection...just a slot assign.

            var slotInfo = new JamManSlotInfo();

            slotInfo.LibItemId = libItem.Id;
            slotInfo.LibItemName = libItem.Name;
            slotInfo.SlotNumber = slotIndex;
            slotInfo.IsEmpty = false;
            slotInfo.LoopInfo = libItem.LoopInfo;

            connectionInfo.Slots[slotIndex - 1] = slotInfo;

            // Save the connection info with this slot assignment!
            if (connectionInfo.Save(mJmConnectionInfoFile, library) == false)
            {
                notify?.Status("JammanAssignSlot: FAILED.  Unable to save connection info with slot assignment.", 1);
                return;
            }

            notify?.Status("JammanAssignSlot: SUCCESS.", 1);

            // Update the cache...
            CacheUpdate(notify);

            notify?.CommandInfo(CommandType.JammanAssignSlot, CommandState.Completed);
        }

        public static void LibraryInit(JMNotifier notify)
        {
            notify?.CommandInfo(CommandType.LibraryInit, CommandState.Started);

            if (Directory.Exists(mJmLibDir) == false)
            {
                try
                {
                    Directory.CreateDirectory(mJmLibDir);
                }
                catch
                {
                    notify?.Status("LibraryInit: FAILED.  Unable to create directory: " + mJmLibDir, 1);
                    return;
                }
            }

            if (File.Exists(mJmLibFile) == false)
            {
                var libraryId = Guid.NewGuid().ToString().Replace("{", "").Replace("}", "");
                var jmcLibrary = new JMCLibrary(libraryId);
                if (jmcLibrary.SaveLibrary(mJmLibFile, false) == false)
                {
                    notify?.Status("LibraryInit: FAILED.  Unable to create file: " + mJmLibFile, 1);
                    return;
                }

                notify?.Status("LibraryInit: New library file created.", 1);
                CacheUpdate(notify);
            }

            if (Directory.Exists(mJmArchiveDir) == false)
            {
                try
                {
                    Directory.CreateDirectory(mJmArchiveDir);
                    notify?.Status("LibraryInit: New archive folder created.", 1);
                }
                catch
                {
                    // Just a warning...the archive folder is purely an optional way to keep the main library somewhat tidy and retain older WAV
                    // files for perhaps another time...
                    notify?.Status($"LibraryInit: WARNING - unable to create {mJmArchiveDir} folder.  May not have permission for this operation.", 1);
                }
            }

            notify?.Status("LibraryInit: SUCCESS.", 1);

            notify?.CommandInfo(CommandType.LibraryInit, CommandState.Completed);
        }

        public static void LibraryMoveItemUp(string[] args, JMNotifier notify)
        {
            //    args[0]            args[1]  
            // jm -libraryMoveItemUp [LIBINDEX]

            notify?.CommandInfo(CommandType.LibraryMoveItemUp, CommandState.Started);

            if (args == null || args.Length != 2)
            {
                notify?.Status("SYNTAX for -libraryMoveItemUp tip: must have the exact parameter of a [LIBINDEX]. Indicies are 1-based.", 1);
                return;
            }

            var oneBasedIndex = -1;
            if (int.TryParse(args[1], out oneBasedIndex) == false)
            {
                notify?.Status("[LIBINDEX] param must not be empty and must be valid.", 1);
                return;
            }

            JMCLibrary library = new JMCLibrary();

            if (library.Load(mJmLibFile) == false)
            {
                notify?.Status($"LibraryMoveItemUp: FAILED. {mJmLibFile} does not exist. \"jm -initAll\" should be called.", 1);
                return;
            }

            if (library.SwapUp(oneBasedIndex - 1))
            {
                // since library items could have been added from the sync we save the library now...
                if (library.SaveLibrary(mJmLibFile, false) == false)
                {
                    notify?.Status("LibraryMoveItemUp: FAILED. SaveLibrary failed. Target file may be LOCKED or this tool does not have WRITE permissions: " + mJmLibFile, 1);
                    return;
                }

                notify?.Status("LibraryMoveItemUp: SUCCESS.", 1);

                // Update the cache...
                CacheUpdate(notify);
            }
            else
            {
                notify?.Status("LibraryMoveItemUp: NO ACTION. (Nothing was swapped/moved since the index was out of range.)", 1);
            }

            notify?.CommandInfo(CommandType.LibraryMoveItemUp, CommandState.Completed);
        }

        public static void LibraryMoveItemDown(string[] args, JMNotifier notify)
        {
            //    args[0]              args[1]  
            // jm -libraryMoveItemDown [LIBINDEX]

            notify?.CommandInfo(CommandType.LibraryMoveItemDown, CommandState.Started);

            if (args == null || args.Length != 2)
            {
                notify?.Status("SYNTAX for -libraryMoveItemDown tip: must have the exact parameter of a [LIBINDEX]. Indicies are 1-based.", 1);
                return;
            }

            var oneBasedIndex = -1;
            if (int.TryParse(args[1], out oneBasedIndex) == false)
            {
                notify?.Status("[LIBINDEX] param must not be empty and must be valid.", 1);
                return;
            }

            JMCLibrary library = new JMCLibrary();

            if (library.Load(mJmLibFile) == false)
            {
                notify?.Status($"LibraryMoveItemDown: FAILED. {mJmLibFile} does not exist. \"jm -initAll\" should be called.", 1);
                return;
            }

            if (library.SwapDown(oneBasedIndex - 1))
            {
                // since library items could have been added from the sync we save the library now...
                if (library.SaveLibrary(mJmLibFile, false) == false)
                {
                    notify?.Status("LibraryMoveItemDown: FAILED. SaveLibrary failed. Target file may be LOCKED or this tool does not have WRITE permissions: " + mJmLibFile, 1);
                    return;
                }

                notify?.Status("LibraryMoveItemDown: SUCCESS.", 1);

                // Update the cache...
                CacheUpdate(notify);
            }
            else
            {
                notify?.Status("LibraryMoveItemDown: NO ACTION. (Nothing was swapped/moved since the index was out of range.)", 1);
            }

            notify?.CommandInfo(CommandType.LibraryMoveItemDown, CommandState.Completed);
        }

        public static void LibraryArchive(string[] args, JMNotifier notify)
        {
            notify?.CommandInfo(CommandType.LibraryArchive, CommandState.Started);

            if (args == null || args.Length != 2)
            {
                ShowLibraryArchiveSyntax(null, notify);
                return;
            }

            var guidOrIndex = args[1].Replace("\"", "");

            if (string.IsNullOrWhiteSpace(guidOrIndex))
            {
                ShowLibraryArchiveSyntax("[GUID | INDEX] param must not be empty.", notify);
                return;
            }

            // Example GUID: afb66faf-09ef-4439-ba3b-31812d718d25
            var libGuid = guidOrIndex.Length > 30 && guidOrIndex.Contains("-") ? guidOrIndex : null;
            var libIndex = -1;

            if (libGuid == null)
            {
                if (int.TryParse(guidOrIndex, out libIndex) == false)
                {
                    ShowLibraryArchiveSyntax("INDEX param detected but could not be parsed. The param must be a valid GUID or INDEX of the library item. INDEX is 1-based.", notify);
                    return;
                }
            }

            if (string.IsNullOrWhiteSpace(libGuid) && libIndex < 1)
            {
                ShowLibraryArchiveSyntax("No valid GUID or INDEX were provided.", notify);
                return;
            }

            // Verify the archive folder exists...should have been created on -initAll
            if (Directory.Exists(mJmArchiveDir) == false)
            {
                notify?.Status($"LibraryArchive: FAILED. The archive directory {mJmArchiveDir} does not exist. Try calling -initAll.", 1);
                return;
            }

            JMCLibrary library = new JMCLibrary();
            if (library.Load(mJmLibFile) == false)
            {
                notify?.Status("LibraryArchive: FAILED. Unable to load existing library.", 1);
                return;
            }

            JMCLibraryItem libItem;
            if (libGuid != null)
            {
                libItem = library.FindItem(libGuid);

                if (libItem == null)
                {
                    notify?.Status("LibraryArchive: FAILED. Unable to find a library item with a matching GUID of: " + libGuid, 1);
                    return;
                }
            }
            else
            {
                var zBasedlibIndex = libIndex - 1;

                if (zBasedlibIndex < 0 || zBasedlibIndex > (library.LibraryItems.Count - 1))
                {
                    notify?.Status("LibraryArchive: FAILED. The INDEX used is out of range.  INDEX: " + libIndex.ToString(), 1);
                    return;
                }

                libItem = library.LibraryItems[zBasedlibIndex];

                if (libItem == null)
                {
                    notify?.Status("LibraryArchive: FAILED. Unable to find a library item with INDEX of: " + libIndex, 1);
                    return;
                }
            }

            string shortNameOnly = SafeShortFileName(libItem.Name);
            shortNameOnly = shortNameOnly.Replace(".wav", "");

            string fullTarget = System.IO.Path.Combine(mJmArchiveDir, shortNameOnly + ".wav");
            string fullTargetText = System.IO.Path.Combine(mJmArchiveDir, shortNameOnly + ".txt"); // new feature for archive: we copy the metadata to a text file
            
            var sbMeta = new StringBuilder();
            sbMeta.AppendFormat("Name: {0}{1}", libItem.Name, Environment.NewLine);
            sbMeta.AppendFormat("Description: {0}{1}", libItem.Desc, Environment.NewLine);
            sbMeta.AppendFormat("Original Source: {0}{1}", libItem.OrigSourceFile, Environment.NewLine);
            sbMeta.AppendFormat("Size: {0}{1}", libItem.Size, Environment.NewLine);
            sbMeta.AppendFormat("Legacy Id: {0}{1}", libItem.Id, Environment.NewLine);
            sbMeta.AppendFormat("File Date: {0}{1}", libItem.TimeStamp.ToString(), Environment.NewLine);
            sbMeta.AppendFormat("Loop Mode: {0}{1}", libItem.LoopInfo.LoopModeType.ToString("g"), Environment.NewLine);
            sbMeta.AppendFormat("Loop Tempo (BPM): {0}{1}", TempoToBPM(libItem.LoopInfo.Tempo), Environment.NewLine);
            sbMeta.AppendFormat("Loop Time Signature: {0}{1}", libItem.LoopInfo.TimeSignatureType.ToString("g"), Environment.NewLine);
            sbMeta.AppendFormat("Loop Rhythm: {0}{1}", libItem.LoopInfo.RhythmType.ToString("g"), Environment.NewLine);
            sbMeta.AppendFormat("Loop Stop Mode: {0}{1}", libItem.LoopInfo.StopModeType.ToString("g"), Environment.NewLine);
            sbMeta.AppendFormat("Loop Raw XML: {0}", Environment.NewLine);
            sbMeta.AppendFormat("{0}", libItem.LoopInfo.ToXML());

            string libShortFile = libItem.Id + ".wav";
            string fullLibSource = Path.Combine(mJmLibDir, libShortFile);

            // Never copy over an existing archive file with the same name...
            string uniqueFullTarget = fullTarget;
            int uniqueOrdinal = 1;
            while (File.Exists(uniqueFullTarget) == true)
            {
                uniqueFullTarget = Path.Combine(mJmArchiveDir, shortNameOnly + uniqueOrdinal.ToString() + ".wav");
                uniqueOrdinal++;
            }

            string uniqueFullTargetText = fullTargetText;
            uniqueOrdinal = 1;
            while (File.Exists(uniqueFullTargetText) == true)
            {
                uniqueFullTargetText = Path.Combine(mJmArchiveDir, shortNameOnly + uniqueOrdinal.ToString() + ".txt");
                uniqueOrdinal++;
            }

            string errorMessage;
            JMCLibrary.FileCopyAsync(fullLibSource, uniqueFullTarget, notify?.Status, out errorMessage);
            JMCLibrary.SaveTextToFile(sbMeta.ToString(), uniqueFullTargetText, notify?.Status, out errorMessage);

            notify?.Status($"LibraryArchive: SUCCESS. {libItem.Name}'s WAV file was copied to the archive folder {mJmArchiveDir}. The meta data was also copied to the archive as a corresponding .txt file so its properties can be re-imported and restored if you want.", 1);

            // There's no reason to update the cache 

            notify?.CommandInfo(CommandType.LibraryArchive, CommandState.Completed);
        }

        public static void LibraryRemove(string[] args, JMNotifier notify)
        {
            //    arg[0]      arg[1]   
            // jm -libraryRemove [GUID | INDEX]

            notify?.CommandInfo(CommandType.LibraryRemove, CommandState.Started);

            if (args == null || args.Length != 2)
            {
                ShowLibraryRemoveSyntax(null, notify);
                return;
            }

            var guidOrIndex = args[1].Replace("\"", "");

            if (string.IsNullOrWhiteSpace(guidOrIndex))
            {
                ShowLibraryRemoveSyntax("[GUID | INDEX] param must not be empty.", notify);
                return;
            }

            // Example GUID: afb66faf-09ef-4439-ba3b-31812d718d25
            var libGuid = guidOrIndex.Length > 30 && guidOrIndex.Contains("-") ? guidOrIndex : null;
            var libIndex = -1;

            if (libGuid == null)
            {
                if (int.TryParse(guidOrIndex, out libIndex) == false)
                {
                    ShowLibraryRemoveSyntax("INDEX param detected but could not be parsed. The param must be a valid GUID or INDEX of the library item. INDEX is 1-based.", notify);
                    return;
                }
            }

            if (string.IsNullOrWhiteSpace(libGuid) && libIndex < 1)
            {
                ShowLibraryRemoveSyntax("No valid GUID or INDEX were provided.", notify);
                return;
            }

            JMCLibrary library = new JMCLibrary();
            if (library.Load(mJmLibFile) == false)
            {
                notify?.Status("LibraryRemove: FAILED. Unable to load existing library.", 1);
                return;
            }

            JMCLibraryItem libItem;
            if (libGuid != null)
            {
                libItem = library.FindItem(libGuid);

                if (libItem == null)
                {
                    notify?.Status("LibraryRemove: FAILED. Unable to find a library item with a matching GUID of: " + libGuid, 1);
                    return;
                }
            }
            else
            {
                var zBasedlibIndex = libIndex - 1;

                if (zBasedlibIndex < 0 || zBasedlibIndex > (library.LibraryItems.Count - 1))
                {
                    notify?.Status("LibraryRemove: FAILED. The INDEX used is out of range.  INDEX: " + libIndex.ToString(), 1);
                    return;
                }

                libItem = library.LibraryItems[zBasedlibIndex];

                if (libItem == null)
                {
                    notify?.Status("LibraryRemove: FAILED. Unable to find a library item with INDEX of: " + libIndex, 1);
                    return;
                }
            }

            ////////////////////////////////////////////////////////////////////////////////////////////////
            // Before removing any library item, we have to (potentially) clear out any slot info...
            var connectionInfo = new JamManConnectInfo();
            if (connectionInfo.Load(mJmConnectionInfoFile, mJamManDir, library) == false)
            {
                notify?.Status("LibraryRemove: FAILED.  Unable to load connection info from file: " + mJmConnectionInfoFile, 1);
                return;
            }

            var slotModsMade = false;
            for (int slotIndex = 0; slotIndex < connectionInfo.Slots.Count; slotIndex++)
            {
                var slot = connectionInfo.Slots[slotIndex];
                if (slot.LibItemId == libItem.Id)
                {
                    connectionInfo.SetEmptySlot(slot.SlotNumber);
                    slotModsMade = true;
                }
            }

            if (slotModsMade)
            {
                if (connectionInfo.Save(mJmConnectionInfoFile, library) == false)
                {
                    notify?.Status("LibraryRemove: ERROR. Unable to remove library reference(s) from the JAMMAN slots. (Failed to save to connection info file.)", 1);
                }
            }
            ////////////////////////////////////////////////////////////////////////////////////////////////

            // Now it's safe to remove from the libary...should not be orphaned on the jamman/slots...
            if (library.DeleteItem(libItem) == false)
            {
                notify?.Status("LibraryRemove: FAILED. Unable to remove item from library with GUID or INDEX value of: " + guidOrIndex, 1);
                return;
            }

            if (library.SaveLibrary(mJmLibFile, false) == false)
            {
                notify?.Status("LibraryRemove: FAILED. SaveLibrary failed. Target file may be LOCKED or this tool does not have WRITE permissions: " + mJmLibFile, 1);
                return;
            }

            notify?.Status("LibraryRemove: SUCCESS. " + libItem.Name + " was removed from the library.", 1);

            // Update the cache
            CacheUpdate(notify);

            notify?.CommandInfo(CommandType.LibraryRemove, CommandState.Completed);
        }

        public static void LibraryUpdate(string[] args, JMNotifier notify)
        {
            //    arg[0]         arg[1]         arg[2]   arg[3]      arg[4]   arg[5] arg[6]        arg[7] arg[8]
            // jm -libraryUpdate [GUID | INDEX] LOOPNAME DESCRIPTION LoopMode Tempo  TimeSignature Rhythm StopMode

            notify?.CommandInfo(CommandType.LibraryUpdate, CommandState.Started);

            if (args == null || args.Length != 9)
            {
                ShowLibraryUpdateSyntax("Invalid number of arguments passed.", notify);
                return;
            }

            var guidOrIndex = args[1].Replace("\"", "");

            if (string.IsNullOrWhiteSpace(guidOrIndex))
            {
                ShowLibraryUpdateSyntax("[GUID | INDEX] param must not be empty.", notify);
                return;
            }

            // Example GUID: afb66faf-09ef-4439-ba3b-31812d718d25
            var libGuid = guidOrIndex.Length > 30 && guidOrIndex.Contains("-") ? guidOrIndex : null;
            var libIndex = -1;

            if (libGuid == null)
            {
                if (int.TryParse(guidOrIndex, out libIndex) == false)
                {
                    ShowLibraryUpdateSyntax("INDEX param detected but could not be parsed. The param must be a valid GUID or INDEX of the library item. INDEX is 1-based.", notify);
                    return;
                }
            }

            if (string.IsNullOrWhiteSpace(libGuid) && libIndex < 1)
            {
                ShowLibraryUpdateSyntax("No valid GUID or INDEX were provided.", notify);
                return;
            }

            JMCLibrary library = new JMCLibrary();
            if (library.Load(mJmLibFile) == false)
            {
                notify?.Status("LibraryUpdate: FAILED. Unable to load existing library.", 1);
                return;
            }

            JMCLibraryItem libItem;
            if (libGuid != null)
            {
                libItem = library.FindItem(libGuid);

                if (libItem == null)
                {
                    notify?.Status("LibraryUpdate: FAILED. Unable to find a library item with a matching GUID of: " + libGuid, 1);
                    return;
                }
            }
            else
            {
                var zBasedlibIndex = libIndex - 1;

                if (zBasedlibIndex < 0 || zBasedlibIndex > (library.LibraryItems.Count - 1))
                {
                    notify?.Status("LibraryUpdate: FAILED. The INDEX used is out of range.  INDEX: " + libIndex.ToString(), 1);
                    return;
                }

                libItem = library.LibraryItems[zBasedlibIndex];
            }

            if (libItem == null)
            {
                notify?.Status("LibraryUpdate: FAILED. Unable to find a library item with INDEX of: " + libIndex, 1);
                return;
            }

            // Gathering the loop arguments - these will need validation and translation to internal values...
            //    arg[0]         arg[1]         arg[2]   arg[3]      arg[4]   arg[5] arg[6]        arg[7] arg[8]
            // jm -libraryUpdate [GUID | INDEX] LOOPNAME DESCRIPTION LoopMode Tempo  TimeSignature Rhythm StopMode
            var argLoopName = args[2].Replace("\"", "");
            var argDesc = args[3].Replace("\"", "");
            var argLoopMode = args[4].Replace("\"", "");
            var argTempo = args[5].Replace("\"", "");
            var argTimeSig = args[6].Replace("\"", "");
            var argRhythm = args[7].Replace("\"", "");
            var argStopMode = args[8].Replace("\"", "");

            // Validate all args...
            if (string.IsNullOrWhiteSpace(argLoopName) ||
                string.IsNullOrWhiteSpace(argDesc) ||
                string.IsNullOrWhiteSpace(argLoopMode) ||
                string.IsNullOrWhiteSpace(argTempo) ||
                string.IsNullOrWhiteSpace(argTimeSig) ||
                string.IsNullOrWhiteSpace(argRhythm) ||
                string.IsNullOrWhiteSpace(argStopMode))
            {
                ShowLibraryUpdateSyntax("One or more arguments are null or empty.", notify);
                return;
            }

            libItem.Name = argLoopName;
            libItem.Desc = argDesc;

            libItem.LoopInfo.LoopModeType = (LoopMode)Enum.Parse(typeof(LoopMode), argLoopMode);

            var iTempo = 0;
            if (int.TryParse(argTempo, out iTempo) == false)
            {
                ShowLibraryUpdateSyntax("Tempo param must be a valid integer > 0", notify);
                return;
            }

            libItem.LoopInfo.Tempo = JMCommands.BPMToTempo(iTempo); // tempo stored is in a specific "tempo" value which is calculated.
            libItem.LoopInfo.TimeSignatureType = (TimeSignature)Enum.Parse(typeof(TimeSignature), argTimeSig);
            libItem.LoopInfo.RhythmType = (Rhythm)Enum.Parse(typeof(Rhythm), argRhythm);
            libItem.LoopInfo.StopModeType = (StopMode)Enum.Parse(typeof(StopMode), argStopMode);

            if (library.SaveLibrary(mJmLibFile, false) == false)
            {
                notify?.Status("LibraryUpdate: FAILED. SaveLibrary failed. Target file may be LOCKED or this tool does not have WRITE permissions: " + mJmLibFile, 1);
                return;
            }

            notify?.Status("LibraryUpdate: SUCCESS.", 1);

            // Update the cache
            CacheUpdate(notify);

            notify?.CommandInfo(CommandType.LibraryUpdate, CommandState.Completed);
        }

        public static void LibraryAdd(string[] args, JMNotifier notify)
        {
            //    arg[0]      arg[1]   arg[2]   arg[3]      arg[4]   arg[5] arg[6]        arg[7] arg[8]
            // jm -libraryAdd FILENAME LOOPNAME DESCRIPTION LoopMode Tempo  TimeSignature Rhythm StopMode

            notify?.CommandInfo(CommandType.LibraryAdd, CommandState.Started);

            if (args == null || args.Length != 9)
            {
                ShowLibraryAddSyntax(null, notify);
                return;
            }

            // Parse all args...
            var fileName = args[1].Replace("\"", "");
            var loopName = args[2].Replace("\"", "");
            var desc = args[3].Replace("\"", "");

            if (string.IsNullOrWhiteSpace(fileName) || File.Exists(fileName) == false)
            {
                ShowLibraryAddSyntax("FILENAME must exist.", notify);
                return;
            }

            if (string.IsNullOrWhiteSpace(loopName))
            {
                ShowLibraryAddSyntax("LOOPNAME must not be empty.", notify);
                return;
            }

            if (string.IsNullOrWhiteSpace(desc))
            {
                ShowLibraryAddSyntax("DESCRIPTION must not be empty.", notify);
                return;
            }

            if (Enum.TryParse<LoopMode>(args[4].Replace("\"", ""), true, out LoopMode loopMode) == false)
            {
                ShowLibraryAddSyntax("Unable to parse LoopMode", notify);
                return;
            }

            if (int.TryParse(args[5].Replace("\"", ""), out int iTempo) == false)
            {
                ShowLibraryAddSyntax("Unable to parse tempo. It should be an integer min of 40 and max of 240", notify);
                return;
            }

            if (Enum.TryParse<TimeSignature>(args[6].Replace("\"", ""), out TimeSignature timeSignature) == false)
            {
                ShowLibraryAddSyntax("Unable to parse TimeSignature.", notify);
                return;
            }

            if (Enum.TryParse<Rhythm>(args[7].Replace("\"", ""), out Rhythm rhythm) == false)
            {
                ShowLibraryAddSyntax("Unable to parse Rhythm.", notify);
                return;
            }

            if (Enum.TryParse<StopMode>(args[8].Replace("\"", ""), out StopMode stopMode) == false)
            {
                ShowLibraryAddSyntax("Unable to parse StopMode.", notify);
                return;
            }

            JMCLibrary library = new JMCLibrary();
            if (library.Load(mJmLibFile) == false)
            {
                notify?.Status("LibraryAdd: FAILED. Unable to load existing library.", 1);
                return;
            }

            var settings = new JMCSettings();
            if (settings.Load(mJmSettingsFile) == false)
            {
                notify?.Status("LibraryAdd: FAILED. Unable to read JMCSettings.xml file.", 1);
                return;
            }

            JamManLoopInfo loopInfo = new JamManLoopInfo();

            loopInfo.LoopModeType = loopMode;
            loopInfo.Tempo = JMCommands.BPMToTempo(iTempo);
            loopInfo.TimeSignatureType = timeSignature;
            loopInfo.RhythmType = rhythm;
            loopInfo.StopModeType = stopMode;

            if (library.AddFileToLibrary(loopInfo, fileName, loopName, desc, notify?.Status, settings) == false)
            {
                notify?.Status("LibraryAdd: FAILED. AddFileToLibrary failed.", 1);
                return;
            }

            if (library.SaveLibrary(mJmLibFile, false) == false)
            {
                notify?.Status("LibraryAdd: FAILED. SaveLibrary failed. Target file may be LOCKED or this tool does not have WRITE permissions: " + mJmLibFile, 1);
                return;
            }

            notify?.Status("LibraryAdd: SUCCESS. " + fileName + " was added to the library.", 1);

            // Update the cache
            CacheUpdate(notify);

            notify?.CommandInfo(CommandType.LibraryAdd, CommandState.Completed);
        }

        public static void LibraryReport(JMNotifier notify)
        {
            notify?.CommandInfo(CommandType.LibraryReport, CommandState.Started);

            JMCLibrary library = new JMCLibrary();
            if (library.Load(mJmLibFile) == false)
            {
                notify?.Status("LibraryReport: FAILED. Unable to load existing library. -initAll may need to be called.", 1);
                return;
            }

            if (library.GenerateLibraryReport(mJmLibReportFile) == false)
            {
                notify?.Status("LibraryReport: FAILED. Unable to generate the Library report file: " + mJmLibReportFile, 1);
                return;
            }

            notify?.Status("LibraryReport: SUCCESS.", 1);
            notify?.CommandInfo(CommandType.LibraryReport, CommandState.Completed);
        }
 
        public static void ShowJammanClearSlotSyntax(string tip, JMNotifier notify)
        {
            if (string.IsNullOrWhiteSpace(tip))
            {
                notify?.Status("SYNTAX for -jammanClearSlot tip: must have the exact parameter of a SLOTINDEX. Indicies are 1-based.", 1);
            }
            else
            {
                notify?.Status("SYNTAX for -jammanClearSlot tip: " + tip, 1);
            }

            notify?.Status("jm -jammanClearSlot [SLOTINDEX]", 1);
        }

        public static void ShowLibraryArchiveSyntax(string tip, JMNotifier notify)
        {
            if (string.IsNullOrWhiteSpace(tip))
            {
                notify?.Status("SYNTAX for -libraryArchive tip: must have the exact parameters. Single param is either the GUID of the library item OR a 1-based integer denoting the index of the item in the library to archive. The archived item will NOT be removed with this action.", 1);
            }
            else
            {
                notify?.Status("SYNTAX for -libraryArchive tip: " + tip, 1);
            }

            notify?.Status("jm -libraryArchive [GUID | INDEX]", 1);
        }

        public static void ShowLibraryRemoveSyntax(string tip, JMNotifier notify)
        {
            if (string.IsNullOrWhiteSpace(tip))
            {
                notify?.Status("SYNTAX for -libraryRemove tip: must have the exact parameters. Single param is either the GUID of the library item OR a 1-based integer denoting the index of the item in the library.", 1);
            }
            else
            {
                notify?.Status("SYNTAX for -libraryRemove tip: " + tip, 1);
            }

            notify?.Status("jm -libraryRemove [GUID | INDEX]", 1);
        }

        public static void ShowLibraryUpdateSyntax(string tip, JMNotifier notify)
        {
            if (string.IsNullOrWhiteSpace(tip))
            {
                notify?.Status("SYNTAX for -libraryUpdate tip: must have the exact parameters.", 1);
            }
            else
            {
                notify?.Status("SYNTAX for -libraryUpdate tip: " + tip, 1);
            }

            notify?.Status("jm -libraryUpdate [GUID | INDEX] LOOPNAME DESCRIPTION LoopMode Tempo TimeSignature Rhythm StopMode", 1);
        }

        public static void ShowLibraryAddSyntax(string tip, JMNotifier notify)
        {
            if (string.IsNullOrWhiteSpace(tip))
            {
                notify?.Status("SYNTAX for -libraryAdd tip: must have the exact parameters. Put FILENAME/LOOPNAME/DESCRIPTION in quotes. Single space between all params.", 1);
            }
            else
            {
                notify?.Status("SYNTAX for -libraryAdd tip: " + tip, 1);
            }

            notify?.Status("jm -libraryAdd FILENAME LOOPNAME DESCRIPTION LoopModeType Tempo TimeSignature Rhythm StopMode", 1);
        }

        public static void ShowJammanAssignSlotSyntax(string tip, JMNotifier notify)
        {
            if (string.IsNullOrWhiteSpace(tip))
            {
                notify?.Status("SYNTAX for -jammanAssignSlot tip: must have the exact parameters. First param of LIBGUID | LIBINDEX, second is the target SLOTINDEX.  Indicies are 1-based.", 1);
            }
            else
            {
                notify?.Status("SYNTAX for -jammanAssignSlot tip: " + tip, 1);
            }

            notify?.Status("jm -jammanAssignSlot [LIBGUID | LIBINDEX] [SLOTINDEX]", 1);
        }

        public static void CacheInit(JMNotifier notify)
        {
            notify?.CommandInfo(CommandType.CacheInit, CommandState.Started);

            if (Directory.Exists(mJmCacheDir) == false)
            {
                try
                {
                    Directory.CreateDirectory(mJmCacheDir);
                }
                catch
                {
                    notify?.Status("CacheInit: FAILED.  Unable to create directory: " + mJmCacheDir, 1);
                    return;
                }
            }

            if (Directory.Exists(mJmUploadsDir) == false)
            {
                try
                {
                    Directory.CreateDirectory(mJmUploadsDir);
                }
                catch
                {
                    notify?.Status("CacheInit: FAILED.  Unable to create directory: " + mJmUploadsDir, 1);
                    return;
                }
            }

            // This will create the existing cache HTML files if they don't already exist
            CacheGenerateHtmlFileIfNotExist(notify);

            notify?.Status("CacheInit: SUCCESS.", 1);

            notify?.CommandInfo(CommandType.CacheInit, CommandState.Completed);
        }

        public static void CacheGenerateHtmlFileIfNotExist(JMNotifier notify)
        {
            notify?.CommandInfo(CommandType.CacheGenerateHtmlFileIfNotExist, CommandState.Started);

            try
            {
                if (File.Exists(mIndexHtmlFile) == false)
                {
                    using (var writer = new StreamWriter(mIndexHtmlFile))
                    {
                        writer.WriteLine("<!DOCTYPE html>");
                        writer.WriteLine("<html>");
                        writer.WriteLine("<h2>JamMan Classic Looper Library Manager</h2>");
                        writer.WriteLine("</html>");
                    }
                }
            }
            catch (Exception x)
            {
                notify?.Status("ERROR: CacheGenerateHtmlFilesIfNotExist threw an exception: " + x.Message, 1);
            }

            notify?.CommandInfo(CommandType.CacheGenerateHtmlFileIfNotExist, CommandState.Completed);
        }

        public static void CacheStatus(JMNotifier notify)
        {
            notify?.CommandInfo(CommandType.CacheStatus, CommandState.Started);

            var allInitialized = true; // assume all initialized

            if (Directory.Exists(mJmCacheDir) == false)
            {
                notify?.Status("CacheStatus (directory check): \\JMCache does not exist. Needs initializing.", 1);
                allInitialized = false;
            }

            if (File.Exists(mIndexHtmlFile) == false)
            {
                notify?.Status("CacheStatus (index.html check): \\JMCache\\index.html does not exist. Needs initializing.", 1);
                allInitialized = false;
            }

            // TODO: there's likely more to check for for the status of the cache - it's what any top layer will depend on
            // so it will be important to verify all assets are initialized...


            if (allInitialized == true)
            {
                notify?.Status("CacheStatus: SUCCESS.", 1);
            }
            else
            {
                notify?.Status("CacheStatus: FAILURE. One or more assets have not been initialized.", 1);
            }

            notify?.CommandInfo(CommandType.CacheStatus, CommandState.Completed);
        }

        public static void CacheUpdateJSON(JMCLibrary library, JamManConnectInfo connectionInfo, JMCSettings settings, JMNotifier notify)
        {
            notify?.CommandInfo(CommandType.CacheUpdateJSON, CommandState.Started);

            var json = string.Empty;

            try
            {
                if (string.IsNullOrWhiteSpace(mJmJson))
                {
                    throw new Exception("Full file path and name for mJmJson file is not defined.");
                }

                if (File.Exists(mJmJson))
                {
                    File.Delete(mJmJson);
                }

                var jamManJSONData = new JamManJSONData(library, connectionInfo, settings);

                json = JsonConvert.SerializeObject(jamManJSONData);
                File.WriteAllText(mJmJson, json);

                // pack the json into the notify event so it can be used by the consumer, e.g., a web client or whatever!
                if (notify != null)
                {
                    notify.Data = json;
                }
            }
            catch (Exception x)
            {
                notify?.Status("CacheUpdateJSON: FAILURE.  Exception thrown.  Details: " + x.Message, 1);
            }

            notify?.CommandInfo(CommandType.CacheUpdateJSON, CommandState.Completed);
        }

        public static void CacheUpdate(JMNotifier notify)
        {
            notify?.CommandInfo(CommandType.CacheUpdate, CommandState.Started);

            JMCLibrary library = new JMCLibrary();

            if (library.Load(mJmLibFile) == false)
            {
                notify?.Status($"CacheUpdate: FAILED. {mJmLibFile} does not exist. \"jm -initAll\" should be called.", 1);
                return;
            }

            var connectionInfo = new JamManConnectInfo();
            if (connectionInfo.Load(mJmConnectionInfoFile, mJamManDir, library) == false)
            {
                notify?.Status("CacheUpdate: FAILED.  Unable to load connection info from file: " + mJmConnectionInfoFile, 1);
                return;
            }

            var settings = new JMCSettings();
            if (settings.Load(mJmSettingsFile) == false)
            {
                notify?.Status("CacheUpdate: FAILED.  Unable to load settings file: " + mJmSettingsFile, 1);
                return;
            }

            ///////////////////////////////////////////////////////////////////////////////////////////////////
            // Write to a JSON file - this is likely what any web or other futuristic UI will sync with...
            CacheUpdateJSON(library, connectionInfo, settings, notify);
            ///////////////////////////////////////////////////////////////////////////////////////////////////


            // The following output is for a static HTML file to show the current state.
            // It's possible to use this tool from the command-line and view this static HTML file
            // and make command-line changes, etc.  It's a slow way to work it, but it works and 
            // it ensures there's always a way to make this tool work regardless of technology changes.
            try
            {
                // Write the cache UI that reflects the state of the current data...
                if (File.Exists(mIndexHtmlFile))
                {
                    File.Delete(mIndexHtmlFile);
                }

                using (var writer = new StreamWriter(mIndexHtmlFile))
                {
                    writer.WriteLine("<!DOCTYPE html>");
                    writer.WriteLine("<html>");
                    writer.WriteLine("<head>");

                    writer.WriteLine("<link rel=\"stylesheet\" href=\"../JMLibraryStyles.css\" />");
                    writer.WriteLine("<link rel=\"stylesheet\" href=\"https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css\" integrity=\"sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh\" crossorigin=\"anonymous\" />");
                    writer.WriteLine("<script src=\"https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js\"></script>");
                    writer.WriteLine("<script src=\"../JMLibraryScript.js\"></script>");
                    writer.WriteLine("</head>");
                    writer.WriteLine("<body>");

                    writer.WriteLine("<div class=\"row row-cols-2 no-gutters\">");

                    /////////////////////////////////////////////////////////////////////                    
                    writer.WriteLine("<div class=\"col-6\">"); // Library Loop Options Here

                    writer.WriteLine("<h2>LOOP PROPERTIES</h2>");
                    writer.WriteLine("<table>");
                    writer.WriteLine("<tr>");
                    writer.WriteLine("<td class=\"loopOptionsCol1\">Loop Name</td>");
                    writer.WriteLine("<td id=\"libSelectedLoopName\">*** Select a loop from the Library ***</td>");
                    writer.WriteLine("</tr>");

                    writer.WriteLine("<tr>");
                    writer.WriteLine("<td>LoopMode</td>");
                    writer.WriteLine("<td id=\"libLoopInfoLoopMode\"></td>");
                    writer.WriteLine("</tr>");

                    writer.WriteLine("<tr>");
                    writer.WriteLine("<td>Tempo</td>");
                    writer.WriteLine("<td id=\"libLoopInfoTempo\"></td>");
                    writer.WriteLine("</tr>");

                    writer.WriteLine("<tr>");
                    writer.WriteLine("<td>Time Signature</td>");
                    writer.WriteLine("<td id=\"libLoopInfoTimeSig\"></td>");
                    writer.WriteLine("</tr>");

                    writer.WriteLine("<tr>");
                    writer.WriteLine("<td>RhythmType</td>");
                    writer.WriteLine("<td id=\"libLoopInfoRhythmType\"></td>");
                    writer.WriteLine("</tr>");

                    writer.WriteLine("<tr>");
                    writer.WriteLine("<td>StopMode</td>");
                    writer.WriteLine("<td id=\"libLoopInfoStopMode\"></td>");
                    writer.WriteLine("</tr>");

                    writer.WriteLine("<tr>");
                    writer.WriteLine("<td>Loop ID</td>");
                    writer.WriteLine("<td id=\"libItemId\"></td>");
                    writer.WriteLine("</tr>");

                    writer.WriteLine("</table>");

                    writer.WriteLine("<h2>{0}</h2>", library.LibraryInfo.Name);

                    writer.WriteLine("</div>");
                    /////////////////////////////////////////////////////////////////////

                    /////////////////////////////////////////////////////////////////////

                    writer.WriteLine("<div class=\"col-6\">"); // JamMan Info Here

                    writer.WriteLine("<h2>COMMANDS</h2>");

                    // The command box is dynamically filled based on the context of any selected library and/or slot along with common commands...
                    writer.WriteLine("<textarea spellcheck=\"false\" class=\"commandBox\"></textarea>");

                    writer.WriteLine($"<h2>{connectionInfo.ConnectionName}</h2>");

                    writer.WriteLine("</div>");

                    /////////////////////////////////////////////////////////////////////
                    writer.WriteLine("<div class=\"col-6 libraryCol\">"); // Start Library Table

                    writer.WriteLine("<table data-library-id=\"{0}\">", library.LibraryInfo.LibraryId); // start table
                    writer.WriteLine("<tr>");
                    writer.WriteLine("<td>#</td>");
                    writer.WriteLine("<td>&nbsp;</td>");
                    writer.WriteLine("<td>Name</td>");
                    writer.WriteLine("<td>Description</td>");
                    writer.WriteLine("<td>Play</td>");
                    writer.WriteLine("</tr>");

                    var index = 0;
                    foreach (var libItem in library.LibraryItems)
                    {
                        // loop details
                        var loopMode = libItem.LoopInfo.LoopModeType.ToString("g");
                        var tempo = libItem.LoopInfo.Tempo.ToString();
                        var timeSig = libItem.LoopInfo.TimeSignatureType.ToString("g");
                        var rhythm = libItem.LoopInfo.RhythmType.ToString("g");
                        var stopMode = libItem.LoopInfo.StopModeType.ToString("g");

                        writer.WriteLine("<tr>");
                        writer.WriteLine("<td>{0}</td>", ++index);
                        writer.WriteLine($"<td><input class=\"libItemRadio\" type=\"radio\" name=\"libraryRadio\" data-libitemid=\"{libItem.Id}\" data-libitemindex=\"{index}\" data-libitemname=\"{libItem.Name}\" data-libitemdesc=\"{libItem.Desc}\" data-loopmode=\"{loopMode}\" data-tempo=\"{tempo}\" data-timesig=\"{timeSig}\" data-rhythm=\"{rhythm}\" data-stopmode=\"{stopMode}\" /></td>");
                        writer.WriteLine("<td>{0}</td>", libItem.Name);
                        writer.WriteLine("<td>{0}</td>", libItem.Desc);

                        // write the audio tag
                        var librarySrcWavFile = Path.Combine(mJmLibDir, libItem.Id + ".wav");
                        writer.WriteLine("<td><audio controls loop><source src=\"{0}\" type=\"audio/wav\" /></audio></td>", librarySrcWavFile);

                        writer.WriteLine("</tr>");
                    }
                    writer.WriteLine("</table>"); // end table

                    writer.WriteLine("</div>");

                    ///////////////////////////////////////////////////////////////////////////////////
                    writer.WriteLine("<div class=\"col-6 jammanCol\">"); // JamMan Slots Here

                    writer.WriteLine("<table>");

                    writer.WriteLine("<tr>");
                    writer.WriteLine("<td class=\"slotCol1\">#</td>");
                    writer.WriteLine("<td class=\"slotCol2\">&nbsp;</td>");
                    writer.WriteLine("<td class=\"slotCol3\">Loop Name</td>");
                    writer.WriteLine("<td class=\"slotCol4\">&nbsp;</td>");
                    writer.WriteLine("</tr>");

                    var slotNum = 1;
                    foreach (var slot in connectionInfo.Slots)
                    {
                        var libItemNameDisplayOnly = string.IsNullOrWhiteSpace(slot.LibItemId) || string.IsNullOrWhiteSpace(slot.LibItemName) ? "[EMPTY]" : slot.LibItemName;

                        writer.WriteLine("<tr>");
                        writer.WriteLine($"<td>{slotNum}</td>");
                        writer.WriteLine($"<td><input class=\"slotLibItemRadio\" type=\"radio\" name=\"slotItemRadio\" data-slotlibitemid=\"{slot.LibItemId}\" data-slotnum=\"{slotNum}\" /></td>");
                        writer.WriteLine($"<td><span data-libitemid=\"{slot.LibItemId}\" data-libitemname=\"{slot.LibItemName}\">{libItemNameDisplayOnly}</span></td>");

                        var librarySrcWavFile = Path.Combine(mJmLibDir, slot.LibItemId + ".wav");
                        writer.WriteLine("<td><audio controls loop><source src=\"{0}\" type=\"audio/wav\" /></audio></td>", librarySrcWavFile);

                        writer.WriteLine("</tr>");

                        slotNum++;
                    }

                    writer.WriteLine("</table>");

                    writer.WriteLine("</div>");


                    writer.WriteLine("</div>"); // end row-cols


                    writer.WriteLine("</body>");
                    writer.WriteLine("</html>");

                    notify?.Status("CacheUpdate: SUCCESS.", 1);
                }
            }
            catch (Exception x)
            {
                notify?.Status("ERROR: CacheUpdate threw an exception: " + x.Message, 1);
            }

            notify?.CommandInfo(CommandType.CacheUpdate, CommandState.Completed);
        }

        public static JamManJSONData GetJamManJSONData()
        {
            JamManJSONData data = null;

            try
            {
                if (string.IsNullOrWhiteSpace(mJmJson))
                {
                    throw new Exception("Full file path and name for mJmJson file is not defined.");
                }

                if (File.Exists(mJmJson) == false)
                {
                    throw new Exception("No existing data file: " + mJmJson);
                }

                // read file into a string and deserialize JSON to a type
                data = JsonConvert.DeserializeObject<JamManJSONData>(File.ReadAllText(mJmJson));
            }
            catch
            {
                data = null;
            }

            return data;
        }

        public static string GetLibraryReportHtml()
        {
            var html = string.Empty;
            try
            {
                if (string.IsNullOrWhiteSpace(mJmLibReportFile))
                {
                    throw new Exception("Full file path and name for mJmLibReportFile file is not defined.");
                }

                if (File.Exists(mJmLibReportFile) == false)
                {
                    throw new Exception("No existing data file: " + mJmLibReportFile);
                }

                // read file into a string....
                html = File.ReadAllText(mJmLibReportFile);
            }
            catch ( Exception x)
            {
                html = "GetLibraryReportHtml() FAILED.  Details: " + x.Message;
            }

            return html;
        }

        public static string GetJamManReportHtml()
        {
            var html = string.Empty;
            try
            {
                if (string.IsNullOrWhiteSpace(mJamManReportFile))
                {
                    throw new Exception("Full file path and name for mJamManReportFile file is not defined.");
                }

                if (File.Exists(mJamManReportFile) == false)
                {
                    throw new Exception("No existing data file: " + mJamManReportFile);
                }

                // read file into a string....
                html = File.ReadAllText(mJamManReportFile);
            }
            catch (Exception x)
            {
                html = "GetJamManReportHtml() FAILED.  Details: " + x.Message;
            }

            return html;
        }

        public static void SettingsInit(JMNotifier notify)
        {
            notify?.CommandInfo(CommandType.SettingsInit, CommandState.Started);

            if (File.Exists(mJmSettingsFile) == false)
            {
                var settings = new JMCSettings
                {
                    LibraryPath = mJmLibDir,
                    AboutBoxDisplayed = true,
                    ConfirmSlotCopy = false,
                    DetectAndImportModifiedSlots = true
                };

                if (settings.Save(mJmSettingsFile) == true)
                {
                    notify?.Status("SettingsInit: SUCCESS. Default JMSettings.xml was generated.", 1);
                }
                else
                {
                    notify?.Status("SettingsInit: FAIL. Unable to generate a default JMSettings.xml file. File may be locked or no permissions granted. " + mJmSettingsFile, 1);
                }
            }
            else
            {
                notify?.Status("SettingsInit: SUCCESS.", 1);
            }

            notify?.CommandInfo(CommandType.SettingsInit, CommandState.Completed);
        }

        public static void ShowHelp(JMNotifier notify)
        {
            notify?.CommandInfo(CommandType.ShowHelp, CommandState.OneShot);
        }

        public static void UnknownCommand(JMNotifier notify)
        {
            notify?.CommandInfo(CommandType.UnknownCommand, CommandState.OneShot);
        }

        private static string SafeShortFileName(string inputName)
        {
            if (string.IsNullOrWhiteSpace(inputName))
            {
                return "Untitled Loop";
            }

            string safeShortFileName = inputName;

            safeShortFileName = safeShortFileName.Replace("*", "");
            safeShortFileName = safeShortFileName.Replace(",", "");
            safeShortFileName = safeShortFileName.Replace("\"", "");
            safeShortFileName = safeShortFileName.Replace("/", "");
            safeShortFileName = safeShortFileName.Replace("[", "");
            safeShortFileName = safeShortFileName.Replace("]", "");
            safeShortFileName = safeShortFileName.Replace(":", "");
            safeShortFileName = safeShortFileName.Replace(";", "");
            safeShortFileName = safeShortFileName.Replace("|", "");
            safeShortFileName = safeShortFileName.Replace("=", "");
            safeShortFileName = safeShortFileName.Replace(".", "");

            return safeShortFileName;
        }

        public static string GetAppPath()
        {
            string codeBase = Assembly.GetExecutingAssembly().Location;
            UriBuilder uri = new UriBuilder(codeBase);
            string path = Uri.UnescapeDataString(uri.Path);
            return Path.GetDirectoryName(path);
        }

        static public int BPMToTempo(int beatsPerMinute)
        {
            if (beatsPerMinute <= 0) return 22050; // don't default to zero!

            return (int)Math.Round(2646000F / (double)beatsPerMinute, MidpointRounding.AwayFromZero);
        }

        static public int TempoToBPM(int tempoSamplesPerSecond)
        {
            if (tempoSamplesPerSecond <= 0) return 120; // don't default to zero!

            return (int)Math.Round(2646000F / (double)tempoSamplesPerSecond, MidpointRounding.AwayFromZero);
        }
    }
}
