using System;
using System.Text;

namespace JamManClassicLib
{
	public class JamManSlotInfo
	{
		private string mLibItemId = "";
		private string mLibItemName = "";
		private JamManLoopInfo mLoopInfo = null; // must be set explicity otherwise it's an empty slot
		private int mSlotNumber = -1;
		private bool mIsEmpty = true; 
		
		public JamManSlotInfo ()
		{
		}

		/// <summary>
		/// When this is blank the slot is empty
		/// </summary>
		public string LibItemId
		{
			get { return mLibItemId; }
			set { mLibItemId = value; }
		}

		public string LibItemName
		{
			get { return mLibItemName; }
			set { mLibItemName = value; }
		}
		
		public JamManLoopInfo LoopInfo
		{
			get { return mLoopInfo; }
			set { mLoopInfo = value; }
		}
				
		public int SlotNumber
		{
			get { return mSlotNumber; }
			set { mSlotNumber = value; }
		}
		
		public bool IsEmpty
		{
			get { return mIsEmpty; }
			set { mIsEmpty = value; }
		}

		public string ToXML(int slotNum)
		{
			SlotNumber = slotNum;

			var xml = new StringBuilder();

			string[] args =
			{
				LoopInfo.LoopModeType.ToString("g"),
				LoopInfo.Tempo.ToString(),
				LoopInfo.TimeSignatureType.ToString("g"),
				LoopInfo.RhythmType.ToString("g"),
				LoopInfo.StopModeType.ToString("g"),
				mLibItemId,
				mLibItemName
			};

			xml.AppendLine(string.Format("<JamManSlotInfo loopMode=\"{0}\" tempo=\"{1}\" timeSignature=\"{2}\" rhythmType=\"{3}\" stopMode=\"{4}\" libItemId=\"{5}\" libItemName=\"{6}\" />", args));

			return xml.ToString();
		}

	}
}

