using System;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Xml;

namespace JamManClassicLib
{
	public class JamManConnectInfo
	{
		private string mJMCSyncFullPathFile = "";
		private bool mHasJMCSyncFile = false;
		private bool mHasLooperToolsFile = false;
		private string mLooperToolsFullPathFile = "";
		private string mJamManFullPath = "";
		private string mShortJMCSyncFileName = "JMSync.xml";
		private string mShortLooperToolsFileName = "LooperTools.xml";
		private string mShortJamManSetupFileName = "SETUP.XML";
		private int mDefaultSlotOnDevice = 1; // when written to SETUP.XML
		private string mConnectionName = ""; // written to/from the JMSync.xml's JMCSync root node
		internal const int SLOTCOUNT = 99; // an internal constant
		private bool mIsModified = false; // flag to detect changes and reflect whether sync needed
		
		private List<JamManSlotInfo> mSlots = new List<JamManSlotInfo>();
		
		public JamManConnectInfo()
		{
		}

		public List<JamManSlotInfo> Slots
		{
			get { return mSlots; }
		}

		public bool AreSlotsClear()
		{
			if (mSlots == null || mSlots.Count == 0)
			{
				return true;
			}

			var libInfoDetected = false;

			foreach ( var slot in mSlots)
			{
				if (slot.IsEmpty == false)
				{
					libInfoDetected = true;
					break;
                }
			}

			return (libInfoDetected == false);
		}

		public bool Save(string connectionXmlFileFullPath, JMCLibrary library)
		{
			var saveResult = false;

			try
			{
				StringBuilder xml = new StringBuilder();

				xml.AppendLine(string.Format("<JamManConnectInfo connectionName=\"{0}\" jamManFullPath=\"{1}\" defaultSlotOnDevice=\"{2}\" libraryId=\"{3}\">", ConnectionName, JamManFullPath, DefaultSlotNumberOnDevice, library.LibraryInfo.LibraryId));
				
				xml.AppendLine("\t<Slots>");
				var slotNum = 1;
				foreach (var slot in mSlots)
				{
					xml.AppendLine(slot.ToXML(slotNum));
					slotNum++;
				}
				xml.AppendLine("\t</Slots>");
				
				xml.AppendLine("</JamManConnectInfo>");

				XmlDocument xmlDoc = new XmlDocument();
				xmlDoc.LoadXml(xml.ToString());

				if (File.Exists(connectionXmlFileFullPath))
				{
					File.Delete(connectionXmlFileFullPath);
				}

				xmlDoc.Save(connectionXmlFileFullPath);

				saveResult = true;
			}
			catch (Exception x)
			{
				saveResult = false;
				var m = x.Message;
			}

			return saveResult;
		}

		public bool Load(string connectionXmlFileFullPath, string jammanPath, JMCLibrary library)
		{
			XmlDocument xdoc = new XmlDocument();

			var loadResult = false;

			SetJamManFullPath(jammanPath);

			try
			{
				InitEmptySlots();

				xdoc.Load(connectionXmlFileFullPath);

				var rootNode = xdoc.DocumentElement; 

				if ( rootNode != null)
				{
					mConnectionName = JMCLibrary.GetSafeAttributeString(rootNode.Attributes["connectionName"]);
					mJamManFullPath = jammanPath; // JMCLibrary.GetSafeAttributeString(rootNode.Attributes["jamManFullPath"]);
					mDefaultSlotOnDevice = JMCLibrary.GetSafeAttributeInt(rootNode.Attributes["defaultSlotOnDevice"]);
				}

				XmlNodeList jmSlotInfoItems = xdoc.SelectNodes("//JamManConnectInfo/Slots/JamManSlotInfo");
				if (jmSlotInfoItems != null && jmSlotInfoItems.Count > 0)
				{
					var slotIndex = 0;
					foreach (XmlNode jmSlotInfo in jmSlotInfoItems)
					{
						JamManSlotInfo jmSlotInfoObj = new JamManSlotInfo();
						jmSlotInfoObj.LoopInfo = JamManLoopInfo.CreateDefaultLoopInfo();

						// Ignore the library id read and always use the one we have loaded...
						//jmSlotInfoObj.LibraryId = library.LibraryInfo.LibraryId;

						jmSlotInfoObj.SlotNumber = slotIndex + 1;
						jmSlotInfoObj.LibItemId = JMCLibrary.GetSafeAttributeString(jmSlotInfo.Attributes["libItemId"]);
						jmSlotInfoObj.LibItemName = JMCLibrary.GetSafeAttributeString(jmSlotInfo.Attributes["libItemName"]);

						jmSlotInfoObj.LoopInfo.LoopModeType = (LoopMode)Enum.Parse(typeof(LoopMode), JMCLibrary.GetSafeAttributeString(jmSlotInfo.Attributes["loopMode"]));
						jmSlotInfoObj.LoopInfo.Tempo = JMCLibrary.GetSafeAttributeInt(jmSlotInfo.Attributes["tempo"]);
						jmSlotInfoObj.LoopInfo.TimeSignatureType = (TimeSignature)Enum.Parse(typeof(TimeSignature), JMCLibrary.GetSafeAttributeString(jmSlotInfo.Attributes["timeSignature"]));
						jmSlotInfoObj.LoopInfo.RhythmType = (Rhythm)Enum.Parse(typeof(Rhythm), JMCLibrary.GetSafeAttributeString(jmSlotInfo.Attributes["rhythmType"]));
						jmSlotInfoObj.LoopInfo.StopModeType = (StopMode)Enum.Parse(typeof(StopMode), JMCLibrary.GetSafeAttributeString(jmSlotInfo.Attributes["stopMode"]));

						// An empty slot is implicit when there's no LibItemId assigned
						jmSlotInfoObj.IsEmpty = string.IsNullOrWhiteSpace(jmSlotInfoObj.LibItemId); 

						mSlots[slotIndex] = jmSlotInfoObj;
						slotIndex++;
					}
				}

				loadResult = true;
			}
			catch (Exception x)
			{
				var m = x.Message;
			}

			return loadResult;
		}
		
		public int 	SlotCount
		{
			get { return SLOTCOUNT; }	
		}
		
		public bool IsModified
		{
			get { return mIsModified; }	
			set { mIsModified = value; }
		}
		
		public bool InSync
		{
			get { return mIsModified == false; }	
		}
		
		public int DefaultSlotNumberOnDevice
		{
			get { return mDefaultSlotOnDevice; }
			set 
			{
				if ( value >= 1 && value <= SlotCount )
				{
					mDefaultSlotOnDevice = value;	
				}
			}
		}
		
		public string ConnectionName
		{
			get { return mConnectionName; }
			set { mConnectionName = value; }
		}

		public void InitEmptySlots()
		{
			// The program should never remove slots - we make them empty!
			// otherwise the slot numbers would get out of whack...
			// But in this case we start be re-allocating a brand
			// new buffer...
			mSlots = null;
			mSlots = new List<JamManSlotInfo>();

			// One-time initialization of the buffer for slots...			
			for (int i = 0; i < SlotCount; i++)
			{
				mSlots.Add(new JamManSlotInfo()); // add a buffer item
				SetEmptySlot(i + 1);
			}

			// we overide the value here
			// as if we're in the context of initialization
			// the SetEmptySlot will set to true.

			// Note that depending on the context, this
			// in the GUI, this flag might indeed get set
			// back to true...
			IsModified = false;
		}

		public bool MakeEmptySlots()
		{
			// This is used when a library item was deleted;
			// the corresponding loop slot must be made empty.
			// Slots are never removed...
			
			if ( mSlots == null || mSlots.Count == 0 )
			{
				return false;	
			}
			
			for ( int i=0; i<mSlots.Count; i++ )
			{
				SetEmptySlot(i+1);
			}
			
			return true;
		}		
		
		public JamManSlotInfo SlotFromIndex(int slotIndex)
		{
			// we're passed a 0-based index, not the slot number!
			if ( slotIndex < 0 || slotIndex > (SLOTCOUNT-1) )
			{
				return null; // we can do nothing, out of bounds!	
			}
			
			return mSlots[slotIndex];
		}
		
		public void SetSlot(JamManSlotInfo slotInfo)
		{
			// NOTE: slotNumber is a 1-based number since that's
			// how slots are known - but we will treat it as an index:
			
			if ( slotInfo == null )
			{
				return;	
			}
			
			int slotIndex = slotInfo.SlotNumber-1;
			
			if ( slotIndex < 0 || slotIndex > (SLOTCOUNT-1) )
			{
				return; // index out of range; we do nothing!	
			}
			
			mSlots[slotIndex] = slotInfo;
			
			IsModified = true;
		}
		
		public void SetEmptySlot(int slotNumber)
		{
			// NOTE: slotNumber is a 1-based number since that's
			// how slots are known - but we will treat it as an index:
			int slotIndex = (slotNumber-1);
			
			if ( slotIndex < 0 || slotIndex > (SlotCount-1) )
			{
				return; // index out of range; we do nothing!	
			}
			
			JamManSlotInfo slotInfo = new JamManSlotInfo();	
				
			slotInfo.LoopInfo = new JamManLoopInfo();
			slotInfo.SlotNumber = slotNumber;
			slotInfo.IsEmpty = true;
			
			mSlots[slotIndex] = slotInfo;
			
			IsModified = true;
		}
		
		public bool SaveSetupFileOnDevice()
		{
			if ( string.IsNullOrWhiteSpace(mJamManFullPath) ||
				 Directory.Exists(mJamManFullPath) == false )
			{
				return false;
			}
			
			bool saveResult = false;
			try
			{
				// This will be a very simple write of the 
				// SETTINGS.XML to the JamMan "Classic" device
				// which takes this format, only saving the loop number value:
				StringBuilder sb = new StringBuilder();
				sb.AppendFormat("<?xml version=\"1.0\" encoding=\"utf-8\" ?>{0}", Environment.NewLine );
				sb.AppendFormat("<JamManSetup>{0}", Environment.NewLine );
				// NOTE: the JamMan's loopnumber value is 0 based!
				sb.AppendFormat("\t<LoopNumber>{0}</LoopNumber>{1}", DefaultSlotNumberOnDevice - 1, Environment.NewLine );
				sb.AppendFormat("</JamManSetup>{0}", Environment.NewLine );
				
				XmlDocument xmlSettings = new XmlDocument();
				xmlSettings.LoadXml(sb.ToString());
				
				string settingsFullPathFile = Path.Combine(mJamManFullPath, mShortJamManSetupFileName );
				if ( File.Exists(settingsFullPathFile) )
				{
					File.Delete(settingsFullPathFile);	
				}
				
				xmlSettings.Save(settingsFullPathFile);
				
				
				saveResult = true;
			}
			catch
			{
				saveResult = false;	
			}
			
			
			return saveResult;
			
		}
		
		public bool SetJamManFullPath(string fullPathOfJamMan)
		{
			if ( string.IsNullOrWhiteSpace(fullPathOfJamMan) ||
				Directory.Exists(fullPathOfJamMan) == false )
			{
				return false;	
			}
			
			this.mJamManFullPath = fullPathOfJamMan;
			this.mJMCSyncFullPathFile = Path.Combine(mJamManFullPath, mShortJMCSyncFileName);
			this.mLooperToolsFullPathFile = Path.Combine(mJamManFullPath, mShortLooperToolsFileName);
			this.mHasJMCSyncFile = File.Exists(mJMCSyncFullPathFile);
			this.mHasLooperToolsFile = File.Exists(mLooperToolsFullPathFile);
			
			// resets modified...
			IsModified = false;
		
			return true;
		}
		
		public string JamManFullPath
		{
			get { return mJamManFullPath; }
		}
		
		public bool HasJMCSyncFile
		{
			get { return mHasJMCSyncFile; }	
		}

		public bool HasLooperToolsFile
		{
			get { return mHasLooperToolsFile; }	
		}
		
		public string JMCSyncFullPathFile
		{
			get { return mJMCSyncFullPathFile; }	
		}
		
		public string LooperToolsFullPathFile
		{
			get { return mLooperToolsFullPathFile; }	
		}
		
		public bool HasLibraryLink (string libItemId)
		{
			if ( string.IsNullOrWhiteSpace(libItemId) ||
				 mSlots == null || 
				 mSlots.Count == 0 )
			{
				return false;	
			}
			
			foreach ( JamManSlotInfo slotInfo in mSlots )
			{
				if ( slotInfo.LibItemId	== libItemId)
				{
					return true;	
				}
			}
			
			return false;
		}
		
		public string GetAudioFileFullPath(int slotNumber, bool mustExist)
		{	
			string loopSubDir = string.Format ( "LOOP{0:00}", slotNumber );
			string loopWavFile = "LOOP.WAV";
			string loopWavFullPath = Path.Combine(mJamManFullPath, loopSubDir, loopWavFile );	
		
			if ( mustExist && File.Exists(loopWavFullPath) == false )
			{
				return string.Empty;	
			}
			
			return loopWavFullPath;
		}

		public bool SwapUp (int slotIndex)
		{
			// slotIndex is zero based!
			if (slotIndex <= 0 || slotIndex > mSlots.Count - 1)
			{
				return false;
			}

			JamManSlotInfo temp = mSlots[slotIndex-1];
			mSlots[slotIndex-1] = mSlots[slotIndex];
			mSlots[slotIndex] = temp;
			
			mSlots[slotIndex-1].SlotNumber--;
			mSlots[slotIndex].SlotNumber++;
			
			IsModified = true;

			return true;
		}
		
		public bool SwapDown (int slotIndex)
		{
			// slotIndex is zero based!
			if (slotIndex < 0 || slotIndex >= mSlots.Count - 1)
			{
				return false;
			}

			JamManSlotInfo temp = mSlots[slotIndex+1];
			mSlots[slotIndex+1] = mSlots[slotIndex];
			mSlots[slotIndex] = temp;
			
			mSlots[slotIndex+1].SlotNumber++;
			mSlots[slotIndex].SlotNumber--;
			
			IsModified = true;

			return true;
		}
	}
}

