﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Media;


namespace JamManClassicLib
{
    public class AudioHelper
    {
        private string mLastError = string.Empty;
        private bool mIsInitialized = false;
        private long mFileSizeKB = -999;
        private int mSampleRate = -999;
        private short mBitsPerSample = -999;
        private int mChannels = -999;
        private DateTime mCreateDate = DateTime.MinValue;
        private string mFile = "";
        private bool mIsOpen = false;
		private bool mLoop = false;

        public AudioHelper()
        {
            
        }
		
		public bool Loop
		{
			get { return mLoop; }
			set { mLoop = value; }
		}

        public string LastError
        {
            get { return mLastError; }
            set { mLastError = value; }
        }

        public long FileSizeKB
        {
            get { return mFileSizeKB; }
        }

        public int SampleRate
        {
            get { return mSampleRate; }
        }

        public short BitsPerSample
        {
            get { return mBitsPerSample; }
        }

        public int Channels
        {
            get { return mChannels; }
        }

        public string ChannelsAsString
        {
            get
            {
                if (mChannels < 0) return "N/A";
                if (mChannels == 0) return "None";
                if (mChannels == 1) return "Mono";
                if (mChannels == 2) return "Stereo";

                return mChannels.ToString();
            }
        }

        public DateTime CreateDate
        {
            get { return mCreateDate; }
            set { mCreateDate = value; }
        }

        public bool IsLoadedAndReady
        {
            get
            {
                return (mIsInitialized == true && File.Exists(mFile));
            }
        }
		
		public void DeInit()
		{
			if ( mIsInitialized )
			{
				mIsInitialized = false;
			}
		}
		
        public bool Init(string file, bool loop)
        {
			DeInit();

            try
            {
				mLoop = loop;
				mFile = file;
				
				if ( string.IsNullOrWhiteSpace(mFile) )
				{
					return false;	
				}

                if (File.Exists(mFile) == false)
                {
                    throw new Exception("File does not exist.");
                }

                if (this.GetMediaProperties(mFile) == false)
                {
                    throw new Exception(this.mLastError);
                }
								
                mIsInitialized = true;
            }
            catch (Exception x)
            {
                mLastError = x.Message;
                mIsInitialized = false;
            }

            return mIsInitialized;
        }

        protected bool Open()
        {
            if (this.mIsOpen)
            {
                return true;
            }

            if (mIsInitialized)
            {
                try
                {
 					this.mIsOpen = true;
                }
                catch 
				{
              
                }
            }

            return this.mIsOpen;
        }

        protected bool GetMediaProperties(string file)
        {
			bool result = false;
			WAVFile wf = null;
			
            try
            {
                if (File.Exists(file) == false)
                {
                    throw new Exception("File does not exist.");
                }

				wf = new WAVFile();
                string strResult = wf.Open(file, WAVFile.WAVFileMode.READ);

                if (string.IsNullOrWhiteSpace(strResult) == false)
                {
                    throw new Exception(strResult);
                }

                this.mSampleRate = wf.SampleRateHz;
                this.mChannels = wf.NumChannels;
                this.mBitsPerSample = wf.BitsPerSample;

                wf.Close();
				wf = null;

                FileInfo fileInfo = new FileInfo(file);

                this.mCreateDate = fileInfo.CreationTime;
                this.mFileSizeKB = (fileInfo.Length / 1024);

                result = true;
            }
            catch ( Exception x )
            {
                result = false;
                this.mLastError = x.Message;
            }
			finally
			{
				if ( wf != null )
				{
					wf.Close();	
					wf = null;	
				}
			}

            return result;
        }


        public bool ConvertTo44100Mono(string targetFile, out string errorMessage, ShowStatus showStatus)
        {
            errorMessage = "";
            
            if (this.mIsInitialized == false)
            {
                errorMessage = "Source audio must be initialized.";
                return false;
            }

            // Check source for limitation
            if (this.mBitsPerSample > 16)
            {
                errorMessage = "Source audio must be 16-bit or less.";
                return false;
            }

            bool wasOpen = this.mIsOpen;

            bool result = false;
            try
            {
                if (File.Exists(targetFile))
                {
                    File.Delete(targetFile);
                }

                // NOTE: the WAV lib can't currently handle 32 bit audio conversions...
                // WAVFile wf = new WAVFile();
                WAVFile.CopyAndConvert(this.mFile, targetFile, 16, false, 1.0, showStatus); // 44100 is presumed and the limitation

                result = true;
            }
            catch (Exception x)
            {
                errorMessage = x.Message;
                result = false;
            }

            if (wasOpen)
            {
                this.Open();
            }

            return result;
        }
    }
}
