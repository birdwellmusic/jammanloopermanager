﻿using System;
using System.Text;
using System.Xml;

namespace JamManClassicLib
{
	public class JamManLoopInfo
    {
        public LoopMode LoopModeType { get; set; }
        public int Tempo { get; set; }  // this is really samples per second at 44100 samples of audio per second
        public TimeSignature TimeSignatureType { get; set; }
        public Rhythm RhythmType { get; set; }
        public StopMode StopModeType { get; set; }
		
		public bool IncludeXMLDeclaration { get; set; }
		
		static public JamManLoopInfo CreateDefaultLoopInfo()
		{
			JamManLoopInfo loopInfo = new JamManLoopInfo();

			loopInfo.Tempo = JMCommands.BPMToTempo(120);

			// NOTE: can override this to default to Single in settings! -RB 12/16/2014
			loopInfo.LoopModeType = LoopMode.Loop; 

			loopInfo.RhythmType = Rhythm.None;
			loopInfo.StopModeType = StopMode.Stop;
			loopInfo.TimeSignatureType = TimeSignature.FourFour;
			
			return loopInfo;
		}

        public string ToXML()
        {
            StringBuilder sb = new StringBuilder();
            
			// NOTE: this XML block is for the individual LOOP01, LOOP02, LOOPnn directory files
			// It is named LOOP.XML - not to be confused with the SETUP.XML which is managed
			// by the Looper Pedal directly...
			
			// Not sure if this is needed - can be included if so...
			if ( IncludeXMLDeclaration )
			{
				sb.AppendLine("<?xml version=\"1.0\" encoding=\"utf-8\" ?>");
			}
            
			sb.AppendLine("<JamManLoopSetup>");

            sb.AppendFormat("\t<LoopMode>{0}</LoopMode>{1}", LoopModeType.ToString("g"), Environment.NewLine);
            sb.AppendFormat("\t<Tempo>{0}</Tempo>{1}", this.Tempo, Environment.NewLine);
            sb.AppendFormat("\t<TimeSignature>{0}</TimeSignature>{1}", (int)this.TimeSignatureType, Environment.NewLine);
            sb.AppendFormat("\t<RhythmType>{0}</RhythmType>{1}", (int)this.RhythmType, Environment.NewLine);
            sb.AppendFormat("\t<StopMode>{0}</StopMode>{1}", StopModeType.ToString("g"), Environment.NewLine);

            sb.AppendLine("</JamManLoopSetup>");

            return sb.ToString();
        }
		
		static public JamManLoopInfo FromXML(string xml)
		{
			JamManLoopInfo loopInfo = JamManLoopInfo.CreateDefaultLoopInfo();
			
			try
			{
				XmlDocument xdoc = new XmlDocument();
				xdoc.LoadXml(xml);
				
				XmlNodeList loopMode = xdoc.SelectNodes("//JamManLoopSetup/LoopMode");
				XmlNodeList tempo = xdoc.SelectNodes("//JamManLoopSetup/Tempo");
				XmlNodeList timeSignature = xdoc.SelectNodes("//JamManLoopSetup/TimeSignature");
				XmlNodeList rhythmType = xdoc.SelectNodes("//JamManLoopSetup/RhythmType");
				XmlNodeList stopMode = xdoc.SelectNodes("//JamManLoopSetup/StopMode");
	
				foreach ( XmlNode node in loopMode )
				{
					loopInfo.LoopModeType = (LoopMode) Enum.Parse(typeof(LoopMode), node.InnerText, true);
				}
				
				foreach ( XmlNode node in tempo )
				{
					loopInfo.Tempo = Convert.ToInt32(node.InnerText);
				}
				
				foreach ( XmlNode node in timeSignature )
				{
					// the enum's ordinal is saved, not the enum string...
					loopInfo.TimeSignatureType = (TimeSignature) Convert.ToInt32(node.InnerText);
				}
				
				foreach ( XmlNode node in rhythmType )
				{
					loopInfo.RhythmType = (Rhythm) Enum.Parse(typeof(Rhythm), node.InnerText, true);
				}
				
				foreach ( XmlNode node in stopMode )
				{
					loopInfo.StopModeType = (StopMode) Enum.Parse(typeof(StopMode), node.InnerText, true);
				}
				
			}
			catch
			{
				loopInfo = null;	
			}
			
			return loopInfo;	
		}
    }
}
