using System;
using System.IO;
using System.Reflection;
using System.Xml;
using System.Text;

namespace JamManClassicLib
{
	public class JMCSettings
	{
		private string mLibraryPath = "";
		private string mRecentJamManDeviceLocation = "";
		private bool mAboutBoxDisplayed = false;
		private bool mDetectAndImportModifiedSlots = false;
		private bool mConfirmSlotCopy = true; // user can turn off warnings, but default is to warn
		private bool mDefaultNewLoopsToSinglePlayMode = false; // user can override the LoopMode for newly imported loops

		public JMCSettings()
		{
			
		}
		
		public string JMCSettingsShortFileName
		{
			get { return "JMSettings.xml"; }
		}
		
		public string JMCLibraryShortFileName
		{
			get { return "JMLibrary.xml"; }
		}
		
		public bool AboutBoxDisplayed
		{
			get { return mAboutBoxDisplayed; }
			set { mAboutBoxDisplayed = value; }
		}
		
		public bool DetectAndImportModifiedSlots
		{
			get { return mDetectAndImportModifiedSlots; }
			set { mDetectAndImportModifiedSlots = value; }
		}
		
		public bool ConfirmSlotCopy
		{
			get { return mConfirmSlotCopy; }
			set { mConfirmSlotCopy = value; }
		}

		public bool DefaultNewLoopsToSinglePlayMode
		{
			get { return mDefaultNewLoopsToSinglePlayMode; }
			set { mDefaultNewLoopsToSinglePlayMode = value; }
		}

		public string LibraryPath
		{
			get { return mLibraryPath; }
			set { mLibraryPath = value; }
		}
		
		public bool IsExistingLibraryDetectedFromPath(string libraryPath)
		{
			if ( string.IsNullOrWhiteSpace(libraryPath) )
			{
				return false;	
			}
			
			string libraryFullFile = System.IO.Path.Combine(libraryPath, this.JMCLibraryShortFileName );
			return File.Exists(libraryFullFile);
		}
		
		public string RecentJamManDeviceLocation
		{
			get { return mRecentJamManDeviceLocation; }
			set { mRecentJamManDeviceLocation = value; }
		}
		
		public string LibraryFile
		{
			get 
			{
				if ( string.IsNullOrWhiteSpace(mLibraryPath) == false )
				{
					return Path.Combine(mLibraryPath, JMCLibraryShortFileName );
				}
				else
				{
					return string.Empty;	
				}
			}
		}
		
		public string ToXML()
		{
			StringBuilder sb = new StringBuilder();
			sb.AppendLine("<JMCSettings>");
			sb.AppendFormat("\t<LibraryPath>{0}</LibraryPath>{1}", mLibraryPath, Environment.NewLine);
			sb.AppendFormat("\t<RecentJamManDevice>{0}</RecentJamManDevice>{1}", mRecentJamManDeviceLocation, Environment.NewLine);
			sb.AppendFormat("\t<AboutBoxDisplayed>{0}</AboutBoxDisplayed>{1}", mAboutBoxDisplayed, Environment.NewLine);
			sb.AppendFormat("\t<DetectAndImportModifiedSlots>{0}</DetectAndImportModifiedSlots>{1}", mDetectAndImportModifiedSlots, Environment.NewLine);
			sb.AppendFormat("\t<ConfirmSlotCopy>{0}</ConfirmSlotCopy>{1}", mConfirmSlotCopy, Environment.NewLine);
			sb.AppendFormat("\t<DefaultNewLoopsToSinglePlayMode>{0}</DefaultNewLoopsToSinglePlayMode>{1}", mDefaultNewLoopsToSinglePlayMode, Environment.NewLine);

			sb.AppendLine("</JMCSettings>");
			
			return sb.ToString();
		}
		
		public bool Save(string jmcSettingsFile)
		{
			bool saveResult = false;
			
			try
			{
				//FileInfo fi = new FileInfo(Assembly.GetExecutingAssembly().Location);
				//string location = fi.DirectoryName;
				//string jmcSettingsFile = System.IO.Path.Combine(location, this.JMCSettingsShortFileName); 	
		
				if ( File.Exists(jmcSettingsFile) )
				{
					File.Delete(jmcSettingsFile);	
				}
				
				XmlDocument xmlDoc = new XmlDocument();
				xmlDoc.LoadXml(this.ToXML());
				xmlDoc.Save(jmcSettingsFile);
				
				saveResult = true;
			}
			catch
			{
				saveResult = false;	
			}
			
			return saveResult;
		}
		
		public bool Load(string jmcSettingsFile)
		{
			bool loadResult = false;
			//FileInfo fi = new FileInfo(Assembly.GetExecutingAssembly().Location);
			//string location = fi.DirectoryName;
			//string jmcSettingsFile = System.IO.Path.Combine(location, this.JMCSettingsShortFileName); 	
			
			if ( string.IsNullOrWhiteSpace(jmcSettingsFile) == false &&
				 File.Exists(jmcSettingsFile) )
			{
				XmlDocument xmlDoc = new XmlDocument();
				
				try
				{
					xmlDoc.Load(jmcSettingsFile);
					
					XmlNode libraryPath = xmlDoc.SelectSingleNode("//JMCSettings/LibraryPath");
					if ( libraryPath != null )
					{
						this.mLibraryPath = libraryPath.InnerText;
					}
		
					XmlNode recentJamManLocation = xmlDoc.SelectSingleNode("//JMCSettings/RecentJamManDevice");
					if ( recentJamManLocation != null )
					{
						this.mRecentJamManDeviceLocation = recentJamManLocation.InnerText;
					}
					
					XmlNode aboutBoxDisplayed = xmlDoc.SelectSingleNode("//JMCSettings/AboutBoxDisplayed");
					if ( aboutBoxDisplayed != null )
					{
						this.mAboutBoxDisplayed = Convert.ToBoolean(aboutBoxDisplayed.InnerText);
					}

					XmlNode detectModSlots = xmlDoc.SelectSingleNode("//JMCSettings/DetectAndImportModifiedSlots");
					if ( detectModSlots != null )
					{
						this.mDetectAndImportModifiedSlots = Convert.ToBoolean(detectModSlots.InnerText);
					}
					
					XmlNode confirmSlotCopy = xmlDoc.SelectSingleNode("//JMCSettings/ConfirmSlotCopy");
					if ( confirmSlotCopy != null )
					{
						this.mConfirmSlotCopy = Convert.ToBoolean(confirmSlotCopy.InnerText);
					}

					XmlNode defaultNewLoopsToSinglePlayMode = xmlDoc.SelectSingleNode("//JMCSettings/DefaultNewLoopsToSinglePlayMode");
					if ( defaultNewLoopsToSinglePlayMode != null )
					{
						this.mDefaultNewLoopsToSinglePlayMode = Convert.ToBoolean(defaultNewLoopsToSinglePlayMode.InnerText);
					}
					
					loadResult= true;
				}
				catch
				{
					loadResult = false;	
				}
			}
		
			return loadResult;
		}
			
	}
}

