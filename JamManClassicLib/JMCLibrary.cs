using System;
using System.IO;
using System.Text;
using System.Web;
using System.Collections.Generic;
using System.Xml;

namespace JamManClassicLib
{
	public delegate void ShowStatus(string message, double percentdone);
	public delegate void FileCopyDelegate(string sourceFile, string destFile, ShowStatus showStatus);
	public delegate void WAVFileConvertAndCopyDelegate(string sourceFile, string destFile, short bitsPerSample, bool isStereo, ShowStatus showStatus);
	public delegate void CommandDelegate(CommandType commandType, CommandState commandState);

	public class JMNotifier
	{
		public CommandDelegate CommandInfo;
		public ShowStatus Status;
		public string Data; // reserved and used to pack data, typically JSON so it can be consumed by a GUID client
	}

	public class JMCLibraryInfo
	{
		protected bool mIsDirty = false;
		protected string mVersion = "1.0";
		protected string mCredits = "JamMan \"Classic\" Looper Manager, by Rob Birdwell - BirdwellMusic.com";
		protected string mName = "";
		protected string mAuthor = "";
		protected bool mConvertWAVToMono = true; // default setting for classic manager
		protected string mLibraryId = ""; 		 // the library ID
		
		public bool IsDirty
		{
			get { return mIsDirty; }
			set { mIsDirty = value; }
		}

		public string Version
		{
			get { return mVersion; }
			set { mVersion = string.IsNullOrWhiteSpace(value) == false ? value : "1.0"; }
		}
		
		public string Name
		{
			get { return mName; }
			set { mName = string.IsNullOrWhiteSpace(value) == false ? value : "My JamMan Looper Manager Library"; }
		}
		
		public string Author
		{
			get { return mAuthor; }
			set { mAuthor = string.IsNullOrWhiteSpace(value) == false ? value : ""; }
		}
		
		public string Credits
		{
			get { return mCredits; }
			set { mCredits = string.IsNullOrWhiteSpace(value) == false ? value : "JamMan Looper Manager, by Rob Birdwell - BirdwellMusic.com"; }
		}
		
		public bool ConvertWAVToMono
		{
			get { return mConvertWAVToMono; }
			set { mConvertWAVToMono = value; }
		}
		
		public string LibraryId
		{
			get { return mLibraryId; }
			set { mLibraryId = value; }
		}
		
		public string ToXML()
		{
			StringBuilder sb = new StringBuilder();	

			string[] args =
			{
				HttpUtility.HtmlEncode(this.mVersion),
				HttpUtility.HtmlEncode(this.mName),
				HttpUtility.HtmlEncode(this.mAuthor),
				HttpUtility.HtmlEncode(this.mCredits),
				mConvertWAVToMono.ToString(),
				HttpUtility.HtmlEncode(this.mLibraryId),
			};
			
			sb.AppendFormat("\t<JMCLibraryInfo version=\"{0}\" name=\"{1}\" author=\"{2}\" credits=\"{3}\" convertWavToMono=\"{4}\" id=\"{5}\" />", args);
			sb.AppendLine();
			
			return sb.ToString();
		}
	}
	
	public class JMCLibraryItem
	{
		private string mId = ""; 			 // a unique ID for the library item.  Corresponds to the imported WAV file + ".wav"
		private string mOrigSourceFile = ""; // captured when imported into the "JMLibrary" directory
		private string mName = ""; 			 // user name of the WAV (may be different from source)
		private string mDesc = ""; 			 // description of the WAV
		private long mSize = 0; 			 // byte size of the library WAV file
		private DateTime mTimeStamp 		= DateTime.MinValue; 		// updated when modified, etc.
		private JamManLoopInfo mLoopInfo 	= JamManLoopInfo.CreateDefaultLoopInfo(); // the data that is written to the JAMMAN pedal
		private int mIndex = -1;
		
		public string Id
		{
			get { return mId; }
			set { mId = value; }
		}

		public int Index
		{
			get { return mIndex;  }
			set { mIndex = value; }
		}
		
		public string OrigSourceFile
		{
			get { return mOrigSourceFile; }
			set { mOrigSourceFile = value; }
		}

		public string Name
		{
			get { return mName; }
			set { mName = value; }
		}		
		
		public string Desc
		{
			get { return mDesc; }
			set { mDesc = value; }
		}
		
		public long Size
		{
			get { return mSize; }
			set { mSize = value; }
		}
		
		public DateTime TimeStamp
		{
			get { return mTimeStamp; }
			set { mTimeStamp = value; }
		}
		
		public JamManLoopInfo LoopInfo
		{
			get { return mLoopInfo; }
			set { mLoopInfo = value; }
		}
		
		public string ToXML()
		{
			object[] args =
			{
				HttpUtility.HtmlEncode(this.mId),	
				HttpUtility.HtmlEncode(this.mOrigSourceFile),
				HttpUtility.HtmlEncode(this.mName),	
				HttpUtility.HtmlEncode(this.mDesc),	
				this.mSize,	
				HttpUtility.HtmlEncode(this.mTimeStamp.ToString()),	
				HttpUtility.HtmlEncode(this.mLoopInfo.ToXML()),
				this.mIndex
			};
				
			return string.Format("\t\t<JMCLibraryItem id=\"{0}\" origsourcefile=\"{1}\" name=\"{2}\" desc=\"{3}\" size=\"{4}\" timestamp=\"{5}\" loopxml=\"{6}\" index=\"{7}\" />", args );
		}

	}
	
	/// <summary>
	/// JMC library - stores the XML for the main "JMLibrary.xml" file.
	/// The WAV files are always imported into a "JMLibrary" folder
	/// to ensure they are converted to MONO 44.1 WAV format and the
	/// properties are then managed here.
	/// </summary>
	public class JMCLibrary
	{
		private JMCLibraryInfo mLibraryInfo = new JMCLibraryInfo();
		private List<JMCLibraryItem> mLibraryItems = new List<JMCLibraryItem>();
		private bool mIsLibraryLoaded = false;
		private string mCurrentLibraryRootPath = "";
		
		public JMCLibrary()
		{
		}

		public JMCLibrary(string id)
		{
			mLibraryInfo.LibraryId = id;
		}

		public void Close()
		{
			mIsLibraryLoaded = false;
			mLibraryInfo = new JMCLibraryInfo();
			mLibraryItems = new List<JMCLibraryItem>();
			mCurrentLibraryRootPath = "";
		}
		
		public string CurrentLibraryRootPath
		{
			get { return mCurrentLibraryRootPath; }
			set { mCurrentLibraryRootPath = value; }
		}

		public bool IsLoaded
		{
			get { return mIsLibraryLoaded; }	
		}
		
		public JMCLibraryInfo LibraryInfo
		{
			get { return mLibraryInfo; }	
		}
		
		public List<JMCLibraryItem> LibraryItems
		{
			get { return mLibraryItems; }	
		}
		
		public JMCLibraryItem FindItem(string id)
		{
			if ( mLibraryItems == null || mLibraryItems.Count == 0 )
			{
				return null;	
			}
			
			foreach ( JMCLibraryItem libItem in mLibraryItems )
			{
				if ( libItem.Id == id )
				{
					return libItem;	
				}
			}
			
			return null;
		}
		
		public bool SaveAllToJamMan(JamManConnectInfo connectionInfo, out string errorMessage, ShowStatus showStatus)
		{
			errorMessage = "";
			
			bool saveResult = false;
			string context = "";
			
			try
			{
				context = "Validating required information.";
				if ( File.Exists(connectionInfo.JMCSyncFullPathFile) == false )
				{
					throw new Exception ( "No JMCSync.xml file detected on the device: " + connectionInfo.JMCSyncFullPathFile);	
				}
				
				if ( Directory.Exists(connectionInfo.JamManFullPath) == false )
				{
					throw new Exception ( "Could not find JamMan directory: " + connectionInfo.JamManFullPath);		
				}

				context = "Clearing existing content from device...";
				/////////////////////////////////////////////////////////////////
				// Begin the critical part - remove ALL LOOP01....LOOP0n folders
				// and files so that we can re-write these!
				DirectoryInfo loopRootDirInfo = new DirectoryInfo(connectionInfo.JamManFullPath);
				DirectoryInfo[] loopDirectories = loopRootDirInfo.GetDirectories();
				
				double percentDone = 0;
				double directoryProcessed = 0;
				foreach ( DirectoryInfo loopDirectory in loopDirectories )
				{
					directoryProcessed++;
					percentDone = directoryProcessed/(double)loopDirectories.Length;
					
					showStatus("Clearing directory: " + loopDirectory.Name, percentDone);
					
					FileInfo[] files = loopDirectory.GetFiles();
					
					foreach ( FileInfo file in files )
					{
						File.Delete(file.FullName);	
					}
					
					// now we can delete the directory
					Directory.Delete(loopDirectory.FullName);
				}
				/////////////////////////////////////////////////////////////////
				
				percentDone = 0;
				
				context = "Re-writing all loop and data to device";
				/////////////////////////////////////////////////////////////////
				// Next step:  re-write all the LOOP01...LOOP0n directories
				// and files per our library settings.  Note that we only need
				// to write FULL slots and ignore the empty slots, if any.
				for ( int slotIndex=0; slotIndex<connectionInfo.SlotCount; slotIndex++)
				{
					JamManSlotInfo slotInfo = connectionInfo.SlotFromIndex(slotIndex);
			
					percentDone = ((double)slotIndex+1)/(double)connectionInfo.SlotCount;
					
					showStatus("Copying files to slot: " + (slotIndex+1).ToString(), percentDone);
					
					// NOTE: no need to write anything to an empty slot
					// on the JamMan device!
					if ( slotInfo.IsEmpty )
					{
						continue; 	
					}
					
					// We have something to write!
					string loopFolderShortName = string.Format("LOOP{0:00}", slotIndex+1);
					string loopFolderFullPath = Path.Combine(connectionInfo.JamManFullPath, loopFolderShortName );
					
					Directory.CreateDirectory(loopFolderFullPath);
					
					if ( Directory.Exists(loopFolderFullPath) == false )
					{
						throw new Exception("Failed to create directory: " + loopFolderFullPath );	
					}

					/////////////////////////////////////////////////////////////
					// Copy the WAV file from the library to the device location
					string librarySrcWavFile = Path.Combine(this.mCurrentLibraryRootPath, slotInfo.LibItemId + ".wav");
					string loopTargetWavFile = Path.Combine(loopFolderFullPath, "LOOP.WAV" );
					
					FileCopyAsync(librarySrcWavFile, loopTargetWavFile, showStatus, out errorMessage);
					
					if ( File.Exists(loopTargetWavFile) == false )
					{
						throw new Exception("Failed to copy loop WAV file to device: " + loopTargetWavFile );			
					}
					
					/////////////////////////////////////////////////////////////
					// Copy the LOOP.XML info from the library to the device location
					string loopXmlTargetFile = Path.Combine(loopFolderFullPath, "LOOP.XML" );
					XmlDocument loopXml = new XmlDocument();

					// when writing to the device we always write the header declaration...
					slotInfo.LoopInfo.IncludeXMLDeclaration = true;

					loopXml.LoadXml(slotInfo.LoopInfo.ToXML()); // FUTURE: if supporting devices other than the JamMan classic, might need different xml
					
					loopXml.Save(loopXmlTargetFile);
					
					if ( File.Exists(loopXmlTargetFile) == false )
					{
						throw new Exception("Failed to copy loop info file to device: " + loopXmlTargetFile );			
					}
				}
				/////////////////////////////////////////////////////////////////
				
				// Save the JamMan's SETTINGS.XML file - this just writes
				// the default loop to play...
				connectionInfo.SaveSetupFileOnDevice();

				// Handle the modified state - should be in sync!
				connectionInfo.IsModified = false;

				// Save the sync file on the device so we know how to sync back if/when files are modified on the road...
				SaveJamManSyncFile(connectionInfo, out errorMessage);

				saveResult = true;
			}
			catch ( Exception x )
			{
				errorMessage = string.Format ( "Error: {0} Details: {1}  Context: {2}", errorMessage, x.Message, context );				
				saveResult = false;	
			}
			
			return saveResult;
		}

		public bool ImportDeviceCreatedLoop (out JMCLibraryItem libItem, JamManConnectInfo connectionInfo, string newLoopWavFullPath, string newLoopXmlFullPath, ShowStatus showStatus)
		{
			libItem = new JMCLibraryItem();
			
			// These are items detected on the JamMan device
			// that were previously empty slots in our sync file
			// but are now showing up as an item on the device -
			// as such, we need to import it into the library and
			// indicate that it's a "new" loop created on the JamMan device
			
			// NOTE: in the case where the this is adding a "revised"
			// item, the caller will modify the Name and Description values
			// per the original file and the changes detected:
			libItem.Id = Guid.NewGuid().ToString();
			libItem.Name = string.Format ( "NEW LOOP {0:yyyy-MM-dd}", DateTime.Now );
			libItem.Desc = "This loop was detected as either a new or orphaned loop on the JamMan.";
			libItem.OrigSourceFile = newLoopWavFullPath;
			libItem.TimeStamp = DateTime.Now;
			bool wasImported = false;
			
			try
			{
				FileInfo wavFileInfo = new FileInfo(newLoopWavFullPath);
				libItem.Size = wavFileInfo.Length;					
				
				// Read the device created loop info...
				XmlDocument loopInfoXml = new XmlDocument();
				loopInfoXml.Load(newLoopXmlFullPath);
				
				libItem.LoopInfo = JamManLoopInfo.FromXML(loopInfoXml.InnerXml);

				// failed to read device created loop info?
				if ( libItem.LoopInfo == null )
				{
					libItem.LoopInfo = JamManLoopInfo.CreateDefaultLoopInfo();	
				}
				
				// Bring in the wave file..
				string libTargetWavFullFile = Path.Combine(mCurrentLibraryRootPath, libItem.Id + ".wav");
				
				string errorMessage; // FUTURE: bubble message if any...
				FileCopyAsync(newLoopWavFullPath, libTargetWavFullFile, showStatus, out errorMessage);
				
				if ( File.Exists(libTargetWavFullFile) )
				{
					// Add this device created item to the library!
					mLibraryItems.Add(libItem);
				
					wasImported = true;
				}
				else 
				{
					// failed to copy the file - that can't be good!
					throw new Exception();	
				}
			}
			catch
			{
				libItem = null;	
				wasImported = false;
			}
			
			return wasImported;
		}
		
		public bool IsSameLibraryAsJamMan ( JamManConnectInfo connectionInfo )
		{
			if ( connectionInfo == null )
			{
				return false;	
			}
			
			try
			{
				// Need to fetch the libraryId and compare it to ours...
				XmlDocument syncXml = new XmlDocument();
				syncXml.Load(connectionInfo.JMCSyncFullPathFile);
				
				XmlNode loopParent = syncXml.SelectSingleNode("//JMCSync/Loops");
				string syncLibId = GetSafeAttributeString(loopParent.Attributes["libraryId"]);
				
				if ( string.IsNullOrWhiteSpace(syncLibId) == false && 
				     syncLibId == this.mLibraryInfo.LibraryId )
				{
					return true;	
				}
			}
			catch
			{
				// possibly missing the libraryId value in the JMCSync.xml file?
				// it's a required value as of 1/2012
			}
			
			return false;
			
		}

		public bool WasSlotChangedOnTheRoad (int slotNumber, JMCLibraryItem libItem, JamManConnectInfo connectionInfo, out bool newRoadFileDetected, out string currentLoopWavFullPath, out string currentLoopXmlFullPath, out JamManLoopInfo revisedLoopInfo, out string changeDetails)
		{
			newRoadFileDetected = false;
			currentLoopWavFullPath = string.Empty;
			currentLoopXmlFullPath = string.Empty;
			revisedLoopInfo = null;
			changeDetails = string.Empty;

			bool slotChangeDetected = false;
			
			try
			{
				string currentLoopFolderName = string.Format ("LOOP{0:00}", slotNumber );
				currentLoopWavFullPath = Path.Combine(connectionInfo.JamManFullPath, currentLoopFolderName, "LOOP.WAV" );
				currentLoopXmlFullPath = Path.Combine(connectionInfo.JamManFullPath, currentLoopFolderName, "LOOP.XML" );
				
				if ( File.Exists(currentLoopWavFullPath) == false || 
					 File.Exists(currentLoopXmlFullPath) == false )
				{
					// out of context - this method is expecting
					// the slot files to be there so we can compare the difference
					// with the corresponding library item...the case
					// where the lib item was defined but the slot was 
					// empty should already been handled - this is not that scenario
					
					return false; // slightly erroneous but we don't handle this condition.
				}
				
				string libraryWav = Path.Combine(this.mCurrentLibraryRootPath, libItem.Id + ".wav");
				
				if ( File.Exists(libraryWav) == false )
				{
					// the library file doesn't exist?
					// that's probably not something we can resolve!
					return false;	
				}
				
				// There are a couple things we look for in a slot change
				// 1) the physical wav file
				// 2) the loop definition: tempi, rhythm, loop type, etc...
				// How the sync process of the app handles these deltas
				// may be a matter of preferences...we're just reporting the data.
				
				// 1) compare file sizes...it matters!
				FileInfo libFileInfo = new FileInfo(libraryWav);
				FileInfo loopFileInfo = new FileInfo(currentLoopWavFullPath);
				
				bool fileSizeDifferent = 
					Math.Abs(libFileInfo.Length-loopFileInfo.Length) > 100; // FUTURE: needs to be a config value?
				
				if ( fileSizeDifferent )
				{
					changeDetails = "File size of loop changed from library version.";
					newRoadFileDetected = true;
				}
				
				// 2) compare the loop meta data including tempo
				XmlDocument currLoopXml = new XmlDocument();
				currLoopXml.Load(currentLoopXmlFullPath);
				
				revisedLoopInfo = JamManLoopInfo.FromXML(currLoopXml.InnerXml);
				
				// compare the loop info from the slot to the library loop info
				// NOTE: the tempo has to be off by > 2 beats to signify a change
				bool loopInfoDifferent = 
					revisedLoopInfo.LoopModeType != libItem.LoopInfo.LoopModeType ||
					revisedLoopInfo.TimeSignatureType != libItem.LoopInfo.TimeSignatureType ||
					revisedLoopInfo.RhythmType != libItem.LoopInfo.RhythmType ||	
					revisedLoopInfo.StopModeType != libItem.LoopInfo.StopModeType ||
					Math.Abs(revisedLoopInfo.Tempo-libItem.LoopInfo.Tempo) > 1; // FUTURE: needs to be a config value?
				
				if ( loopInfoDifferent )
				{
					if ( changeDetails.Length > 0 )
					{
						changeDetails += "  ";	
					}
					
					changeDetails += "Loop details were revised (tempo, time sig, etc.).";
				}
				
				slotChangeDetected = (fileSizeDifferent || loopInfoDifferent);
				
			}
			catch
			{
				// unable to detect...something failed
				slotChangeDetected = false;	
			}
			
			return slotChangeDetected;
		}
		
		public bool LoadFromJMCSync(JamManConnectInfo connectionInfo, JMCSettings settings, JMNotifier notify)
		{
			var jmcSyncResult = false;
			double percentDone = .1;
			var context = "Reading JMSync info...";
			var roadSlotChangesDetected = false; // if so, we'll re-save the connection info's slots to reflect the current state of the device
			
			if ( connectionInfo == null )
			{
				return false;	
			}
			
			try
			{
				notify?.Status(context, percentDone);
				
				// Re-allocate an array of empty slots...
				connectionInfo.InitEmptySlots();
				
				// Read the looper tools data
				XmlDocument syncXml = new XmlDocument();
				syncXml.Load(connectionInfo.JMCSyncFullPathFile);
				
				XmlNode root = syncXml.SelectSingleNode("//JMCSync");
				
				if ( root == null )
				{
					throw new Exception ( "No JMCSync root node defined.");		
				}
				
				// Read in the JamMan's connection name:
				connectionInfo.ConnectionName = GetSafeAttributeString(root.Attributes["connectionName"]);
				
				// NOTE: this "IsModified" state is retained in the context
				// of the inSync value - we must retain this to ensure 
				// the user clicks the "Save" button which commits all the values
				// to the device...
				connectionInfo.IsModified = !GetSafeAttributeBool(root.Attributes["inSync"]); 
				
				// Read the list of loops...
				XmlNodeList syncLoopList = syncXml.SelectNodes("//JMCSync/Loops/Loop");
				
				if ( syncLoopList == null || syncLoopList.Count == 0 )
				{
					throw new Exception ( "No JMCSync loops defined.");	
				}
				
				int slotNumber = 0;
				
				// The sync flag will change so when loading
				// we have to capture it here and use it 
				// while performing the load logic...
				bool inSyncAtStart = connectionInfo.InSync;
				
				foreach ( XmlNode loopInfo in syncLoopList )
				{
					slotNumber++; // always 1-based!
					percentDone = ((double)slotNumber)/(double)syncLoopList.Count;

					context = string.Format ( "Loading slot {0:00} details", slotNumber );
					notify?.Status(context, percentDone );
					
					// NOTE: every item MUST have a corresponding library item
					// this concept changed 12/30/2011!
					
					string libId = GetSafeAttributeString(loopInfo.Attributes["id"]);
					JMCLibraryItem libItem = FindItem(libId);
					
					// if the loop has no id or no longer exists in the library
					// then we either have to import it as a device created 
					// loop or mark the item as orphaned (or empty)
					
					if ( string.IsNullOrWhiteSpace(libId) || libItem == null)
					{
						//////////////////////////////////////////////////////
						// ALERT: the JMCSync.xml data reports the slot is empty,
						// but did the user create a new slot on the device?
						// If so, we must detect this, import the WAV file to 
						// the library and the loop info:
						
						// NOTE: this was a bug fix - we have to
						// compare the sync status with what it was 
						// prior to the re-sync process...
						if ( inSyncAtStart )
						{
							// NOTE: we only do this if the device
							// was reported "inSync" but now there are
							// clear loops here that need to migrate to 
							// the library...
							
							string newLoopFolderName = string.Format ("LOOP{0:00}", slotNumber );
							string newLoopWavFullPath = Path.Combine(connectionInfo.JamManFullPath, newLoopFolderName, "LOOP.WAV" );
							string newLoopXmlFullPath = Path.Combine(connectionInfo.JamManFullPath, newLoopFolderName, "LOOP.XML" );
							
							if ( File.Exists(newLoopWavFullPath) && File.Exists(newLoopXmlFullPath) )
							{
								context = string.Format ("New device created loop detected - importing into Slot {0}", slotNumber );
								notify?.Status(context, percentDone );
								ImportDeviceCreatedLoop(out libItem, connectionInfo, newLoopWavFullPath, newLoopXmlFullPath, notify?.Status);
								roadSlotChangesDetected = true;
							}
						}
						//////////////////////////////////////////////////////
						
						if ( libItem == null ) // a truly empty slot (or unable to import device created slot!?)
						{
							roadSlotChangesDetected = true;
							connectionInfo.SetEmptySlot(slotNumber);
							continue;	
						}
					}
					
					if ( libItem == null ) // no "device created loop" detected
					{
						// FUTURE: this is believed to simply be
						// a truly empty slot...but reserved for future
						// "smart sync" detections...
						roadSlotChangesDetected = true;
						connectionInfo.SetEmptySlot(slotNumber);
						continue;	
					}

					// If this was a device created item, it was already
					// assigned these and other values - otherwise, load
					// from the loop info that we read...
					if (libItem != null && string.IsNullOrWhiteSpace(libItem.Name) )
					{
						libItem.Name = GetSafeAttributeString(loopInfo.Attributes["name"]);
						libItem.Desc = GetSafeAttributeString(loopInfo.Attributes["desc"]);
						libItem.OrigSourceFile = GetSafeAttributeString(loopInfo.Attributes["originfile"]);
						libItem.TimeStamp = GetSafeAttributeDateTime(loopInfo.Attributes["timestamp"]);
						libItem.Size = GetSafeAttributeLong(loopInfo.Attributes["size"]);
					}
					
					if ( inSyncAtStart && 
						 settings.DetectAndImportModifiedSlots )
					{
						// TODO: new feature and needs lots of testing!
						// It can be enabled/disabled via the settings. Will be tested in 2023! -RB
						
						string changeReport;
						string currentLoopWavFullPath;
						string currentLoopXmlFullPath;
						JamManLoopInfo revisedLoopInfo;
						bool newRoadFileDetected;
						if ( WasSlotChangedOnTheRoad(slotNumber, libItem, connectionInfo, out newRoadFileDetected, out currentLoopWavFullPath, out currentLoopXmlFullPath, out revisedLoopInfo, out changeReport) )
						{
							roadSlotChangesDetected = true;

							if (newRoadFileDetected)
							{
								context = string.Format("Revised loop on device - importing into into library {0}", slotNumber);
								notify?.Status(context, percentDone);
								JMCLibraryItem revisedLibItem;
								if (ImportDeviceCreatedLoop(out revisedLibItem, connectionInfo, currentLoopWavFullPath, currentLoopXmlFullPath, notify?.Status))
								{
									// NOTE: the item was added to the library and in
									// memory - but we need to modify the change information.
									revisedLibItem.Name = libItem.Name + " (Revised " + DateTime.Now.ToShortDateString() + ")";
									revisedLibItem.Desc = libItem.Desc + " *** " + changeReport;

									if (revisedLoopInfo != null)
									{
										revisedLibItem.LoopInfo = revisedLoopInfo;
									}
								}
							}
							else
							{
								// a change to only the meta data - won't be importing a new file
								// but note that the meta data was revised...no need to append any info though...it should reflect desire of the road!
								if (libItem != null)
								{
									context = string.Format("Revised loop on device - changes to loop meta data only to slot # {0}", slotNumber);
									libItem.LoopInfo = revisedLoopInfo;
								}
							}
						}
					}
			
					// Add to JamMan slot...
					JamManSlotInfo slotInfo = new JamManSlotInfo();

					slotInfo.LibItemId = libItem.Id;
					slotInfo.LibItemName = libItem.Name; // will be the actual name or the revised name if it is a road created loop slot.
					slotInfo.SlotNumber = slotNumber;
					slotInfo.LoopInfo = libItem.LoopInfo;
					slotInfo.IsEmpty = false;
					
					connectionInfo.SetSlot(slotInfo);
				}
				
				// NOTE: the IsModified/inSync flag was loaded
				// in this process - we DO NOT set IsModified
				// here as the serialized value will ensure the
				// state of sync and whether or not values need
				// a final commit/save to the device before taking
				// on the road...
				
				// Loaded Successfully!
				jmcSyncResult = true;

				if (roadSlotChangesDetected)
				{
					if (SaveJamManSyncFile(connectionInfo, out string errorMessage) == false)
					{
						notify?.Status("JMSync WARNING: unable to save the connection info slot details that were detected changed from the road. Error: " + errorMessage, 0);
					}
					else
					{
						notify?.Status("JMSync INFO: road changes to slot items detected and were sync'd.", 0);
					}
				}

			}
			catch (Exception x )
			{
				context = string.Format ( "{0} - Details: {1}", context, x.Message );
				jmcSyncResult = false;
			}
			finally
			{
				if ( jmcSyncResult )
				{
					notify?.Status("JMSync successfully loaded!", 0);	
				}
				else
				{
					notify?.Status("JMSync load failed.  Details: " + context, 0);	
				}
			}
			
			return jmcSyncResult;			
		}
		
		private bool NeedsConverting(string sourceFile)
		{
			// A simple I/O optimization: only do conversions
			// if absolutely necessary...and this means even 
			// checking at the I/O level...
			
			try
			{
				if ( this.mLibraryInfo.ConvertWAVToMono == false )
				{
					// optimized: no I/O needed to check!
					return false;	
				}
				else
				{
					WAVFormat wavFormat = WAVFile.GetAudioFormat(sourceFile);
					bool needsMonoConvert = ( wavFormat.BitsPerSample != 16 ||
												  wavFormat.IsStereo == true ||
								             	  wavFormat.SampleRateHz != 44100 );
					
					return needsMonoConvert;
				}
				
				// FUTURE: add additional conversion options checks if/when
				// this librarian supports stereo...
			}
			catch
			{
				// eat it...	
			}
			
			return false;
		}
		
		
		public bool ImportFromUnmanagedDevice (JamManConnectInfo connectionInfo, ShowStatus showStatus)
		{
			bool importResult = false;
			double percentDone = .1;
			string context = "Reading looper info...";
			string errorMessage = string.Empty;
			
			try
			{
				showStatus(context, percentDone);
				
				// Clear any existing slot info:
				connectionInfo.InitEmptySlots();
				
				// Read the JAMMAN folder data
				string[] loopFolders = Directory.GetDirectories(connectionInfo.JamManFullPath);
				
				if ( loopFolders == null || loopFolders.Length == 0)
				{
					// no folders on the jamman device
					// so we must fake it an create a buffer
					// of 99 empty loop folders
					loopFolders = new string[99];
					for ( int i=0; i<loopFolders.Length; i++ )
					{
						loopFolders[i] = string.Empty;	
					}
				}
				
				int slotNumber = 0;
				
				// NOTE: we're not trying to import empty slots
				// FUTURE: consider reading the folders and if there
				// are missing ones, write the empty slot.
				
				foreach ( string loopFolder in loopFolders )
				{
					slotNumber++;
					
					if ( slotNumber >= JamManConnectInfo.SLOTCOUNT )
					{
						// safety check: there were more than 99 folders
						// on this device...bail out!
						break;	
					}
		
					percentDone = ((double)slotNumber)/(double)loopFolders.Length;

					context = string.Format ( "Importing loop to slot {0:00}", slotNumber );
					showStatus(context, percentDone );
					
					// NOTE: a blank loopFolder will be treated as a missing folder...
					string loopWavFile = Path.Combine(loopFolder, "LOOP.WAV");
					
					// Is this an empty slot...
					if ( string.IsNullOrWhiteSpace(loopFolder) || 
					     File.Exists(loopWavFile) == false )
					{
						connectionInfo.SetEmptySlot(slotNumber);
						continue;	
					}
					
					// This JAMMAN has a loop and we need to import it into the
					// library for safe keeping!
					
					JMCLibraryItem libItem = new JMCLibraryItem();
					
					FileInfo fileInfo = new FileInfo(loopWavFile);
					
					libItem.Id = Guid.NewGuid().ToString();
					libItem.Name = string.Format ( "LOOP{0:00}", slotNumber );
					libItem.Desc = ""; // we don't have a description...less might be best
					libItem.OrigSourceFile = loopWavFile;
					libItem.TimeStamp = DateTime.Now;
					libItem.Size = fileInfo.Length;
					
					// Now we need to read in the JamMan's corresponding "LOOP0n" 
					// directory to get the loop settings...

					context = string.Format ( "Importing LOOP.XML for slot {0:00}", slotNumber );
					
					string loopXmlFile = Path.Combine(loopFolder, "LOOP.XML");
					
					if ( File.Exists(loopXmlFile) )
					{
						XmlDocument loopXml = new XmlDocument();
						loopXml.Load(loopXmlFile);
						
						libItem.LoopInfo = JamManLoopInfo.FromXML(loopXml.InnerXml);
					}
					else
					{
						// missing LOOP.XML? Use defaults...
						libItem.LoopInfo = JamManLoopInfo.CreateDefaultLoopInfo(); 
					}
					
					context = string.Format ( "Importing LOOP.WAV to slot {0:00}", slotNumber );
					string shortLibFileName = libItem.Id + ".wav";
					string libWavFullFileName = System.IO.Path.Combine(mCurrentLibraryRootPath, shortLibFileName);
					
					if ( NeedsConverting(loopWavFile) )
					{
						// FUTURE: might need to get back the type of conversion needed.
						// For now we only support one conversion type, mono, 16-bit 44.1
						WAVFileConvertAndCopyAsync(loopWavFile, libWavFullFileName, 16, false, showStatus);
					}
					else
					{
						FileCopyAsync(loopWavFile, libWavFullFileName, showStatus, out errorMessage);
					}
					
					if ( File.Exists(libWavFullFileName) )
					{
						// Add to library...
						mLibraryItems.Add(libItem);
						
						// Add to JamMan slot...
						JamManSlotInfo slotInfo = new JamManSlotInfo();

						slotInfo.LibItemId = libItem.Id;
						slotInfo.LibItemName = libItem.Name;
						slotInfo.SlotNumber = slotNumber;
						slotInfo.LoopInfo = libItem.LoopInfo;
						slotInfo.IsEmpty = false;
						
						connectionInfo.SetSlot(slotInfo);
					}
					else
					{
						throw new Exception("Failed to copy WAV to " + libWavFullFileName);	
					}
				}
				
				// Set the connection sync info...
				connectionInfo.IsModified = false;
				
				context = "Attempting to save JMCSync.xml to device.";
				// Import was successful - write our own JMCSync.xml file
				// to the device...
				if ( SaveJamManSyncFile(connectionInfo, out errorMessage) == false )
				{
					throw new Exception ( "Save failed.  Details: " + errorMessage );		
				}
				
				// Imported Successfully!
				importResult = true;
			}
			catch (Exception x )
			{
				context = string.Format ( "Error: {0} Context: {1} Details: {2}", errorMessage, context, x.Message );
				importResult= false;
			}
			finally
			{
				if ( importResult )
				{
					showStatus("Import successfully completed!", 0);	
				}
				else
				{
					showStatus("Import failed.  Details: " + context, 0);	
				}
			}
			
			return importResult;
		}		
		
		
		public bool ImportFromLooperTools (JamManConnectInfo connectionInfo, ShowStatus showStatus)
		{
			bool importResult = false;
			double percentDone = .1;
			string context = "Reading looper tools info...";
			string errorMessage = string.Empty;
			
			try
			{
				showStatus(context, percentDone);
				
				// Clear any existing slot info:
				connectionInfo.InitEmptySlots();
				
				// Read the looper tools data
				XmlDocument ltXml = new XmlDocument();
				ltXml.Load(connectionInfo.LooperToolsFullPathFile);
				
				XmlNodeList ltLoopList = ltXml.SelectNodes("//root/loops/loop");
				
				// This would be strange - there are no loops
				// defined in the XML...in this case, the SAFE
				// thing to do is to tell the user to rename
				// the LooperTools.xml file and try importing
				// from the device directly - that way no files
				// get lost...meta data is clearly missing...
				if ( ltLoopList == null || ltLoopList.Count == 0)
				{
					throw new Exception ( "There are no loop definitions in the LooperTools.xml file.  You should rename the LooperTools.xml file and try connecting to the device again.");
				}
				
				int slotNumber = 0;
				
				foreach ( XmlNode ltLoopInfo in ltLoopList )
				{
					slotNumber++;
					percentDone = ((double)slotNumber)/(double)ltLoopList.Count;

					context = string.Format ( "Importing slot {0:00} details", slotNumber );
					showStatus(context, percentDone );
					
					string loopShortFolder = string.Format("LOOP{0:00}", slotNumber);
					string loopSlotFullPath = Path.Combine(connectionInfo.JamManFullPath, loopShortFolder);
					
					string loopXmlFile = Path.Combine(loopSlotFullPath, "LOOP.XML");
					string loopWavFile = Path.Combine(loopSlotFullPath, "LOOP.WAV");
					
					// Is this an empty slot...
					if ( File.Exists(loopWavFile) == false ) 
					{
						connectionInfo.SetEmptySlot(slotNumber);
						continue;	
					}
					
					// The LooperTools *might* have a name for this
					// loop or it may have been a device created loop
					// that never got back into the LooperTools library...
					
					JMCLibraryItem libItem = new JMCLibraryItem(); 
					
					libItem.Id = Guid.NewGuid().ToString();
					libItem.Name = GetSafeAttributeString(ltLoopInfo.Attributes["desc"]);
					libItem.Desc = ""; // LooperTools didn't provide what we call a description - it got mapped to the Name
					
					// if there's no name then in all likelihood
					// the loop was created on the device and never part
					// of the LooperTools management...
					if ( libItem.Name == string.Empty )
					{
						libItem.Name = string.Format ( "NEW LOOP {0:yyyy-MM-dd}", DateTime.Now );
						libItem.Desc = "This loop was detected as either a new or orphaned loop on the JamMan.";
						
						// we'll get this info - this file does exist from
						// our check above...
						FileInfo loopWaveFileInfo = new FileInfo(loopWavFile);
						libItem.OrigSourceFile = loopWavFile;
						libItem.TimeStamp = loopWaveFileInfo.CreationTime; // NOTE: this date comes from the device file system that the JamMan wrote to...so it might not be in our epoch.
						libItem.Size = loopWaveFileInfo.Length;
					}
					else
					{
						// possibly LooperTools had this info..
						libItem.OrigSourceFile = GetSafeAttributeString(ltLoopInfo.Attributes["srcpath"]);
						libItem.TimeStamp = GetSafeAttributeDateTime(ltLoopInfo.Attributes["timestamp"]);
						libItem.Size = GetSafeAttributeLong(ltLoopInfo.Attributes["size"]);
					}
					
					// Now we need to read in the JamMan's corresponding "LOOP0n" 
					// directory to get the loop settings...

					context = string.Format ( "Importing LOOP.XML to slot {0:00}", slotNumber );
					
					XmlDocument loopXml = new XmlDocument();
					loopXml.Load(loopXmlFile);
					
					libItem.LoopInfo = JamManLoopInfo.FromXML(loopXml.InnerXml);
					if ( libItem.LoopInfo == null )
					{
						// default to something!	
						libItem.LoopInfo = JamManLoopInfo.CreateDefaultLoopInfo(); 
					}
					
					context = string.Format ( "Importing LOOP.WAV to slot {0:00}", slotNumber );
					string shortLibFileName = libItem.Id + ".wav";
					string libWavFullFileName = System.IO.Path.Combine(mCurrentLibraryRootPath, shortLibFileName);

					// NOTE: a "LooperTools" managed device should have
					// been completely mono files - so we are NOT
					// checking to convert these files as the straight
					// copy should be much faster.
					// FUTURE: add direct ways to see the format of a file
					// and convert one or multiple files on demand when in the lib.
					FileCopyAsync(loopWavFile, libWavFullFileName, showStatus, out errorMessage);
					
					if ( File.Exists(libWavFullFileName) )
					{
						// Add to library...
						mLibraryItems.Add(libItem);
						
						// Add to JamMan slot...
						JamManSlotInfo slotInfo = new JamManSlotInfo();
						
						slotInfo.LibItemId = libItem.Id;
						slotInfo.LibItemName = libItem.Name;
						slotInfo.SlotNumber = slotNumber;
						slotInfo.LoopInfo = libItem.LoopInfo;
						slotInfo.IsEmpty = false;
						
						connectionInfo.SetSlot(slotInfo);
					}
					else
					{
						throw new Exception("Failed to copy WAV file to " + libWavFullFileName );	
					}
				}
				
					
				// Set the connection sync info...
				connectionInfo.IsModified = false;
				
				// Import was successful - write our own JMCSync.xml file to the device...
				context = "Attempting to save the JMCSync.xml to the device.";
				if ( SaveJamManSyncFile(connectionInfo, out errorMessage) == false )
				{
					throw new Exception ( "Save failed.  Details: " + errorMessage );	
				}
		
				// Imported Successfully!
				importResult = true;
			}
			catch (Exception x )
			{
				context = string.Format ( "Error: {0} Context: {1} Details: {2}", errorMessage, context, x.Message );
				importResult= false;
			}
			finally
			{
				if ( importResult )
				{
					showStatus("Import successfully completed!", 0);	
				}
				else
				{
					showStatus("Import failed.  Details: " + context, 0);	
				}
			}
			
			return importResult;
		}

		public static bool SaveTextToFile(string textContents, string uniqueFullTargetText, ShowStatus status, out string errorMessage)
		{
			errorMessage = string.Empty;
			if (File.Exists(uniqueFullTargetText))
			{
				status?.Invoke("SaveTextToFile: the target file cannot already exist.", 1F);
				errorMessage = "SaveTextToFile: the target file cannot already exist.";
				return false;
			}

			var result = false;
			try
			{
				File.WriteAllText(uniqueFullTargetText, textContents);
				status?.Invoke("SaveTextToFile: SUCCESS: " + uniqueFullTargetText, 1F);
				result = true;
			}
			catch (Exception x)
			{
				status?.Invoke("SaveTextToFile: FAILURE: " + x.Message, 1F);
			}

			return result;
		}

		public bool SaveJamManSyncFile(JamManConnectInfo connectionInfo, out string errorMessage)
		{
			errorMessage = "";
			
			bool saveResult = false;
			
			if ( connectionInfo == null)
			{
				return false;	
			}
			
			try
			{
				// FUTURE: consider creating a backup first...
				// Remove existing sync file as we're going to re-write:
				if ( File.Exists(connectionInfo.JMCSyncFullPathFile) )
				{
					File.Delete(connectionInfo.JMCSyncFullPathFile);	
				}

				// Does the file still exist?
				if ( File.Exists(connectionInfo.JMCSyncFullPathFile) )
				{
					throw new Exception ("Unable re-write the JMCSync.xml file on the JamMan device!" );	
				}
		
				StringBuilder sb = new StringBuilder();
				
				sb.AppendFormat("<JMCSync connectionName=\"{0}\" inSync=\"{1}\">", HttpUtility.HtmlEncode(connectionInfo.ConnectionName), connectionInfo.InSync );
				sb.AppendLine();
				sb.AppendFormat("\t<Loops libraryId=\"{0}\">", mLibraryInfo.LibraryId );
				sb.AppendLine();
				
				for ( int slotIndex = 0; slotIndex<connectionInfo.SlotCount; slotIndex++ )
				{
					JamManSlotInfo slotInfo = connectionInfo.SlotFromIndex(slotIndex);
					
					// assumption is an empty slot, unless libItem is detected!
					string name = ""; 
					string desc = "";
					long size = 0;
					string originFile = "";
					
					// if there's a library associated, then get that information:
					JMCLibraryItem libItem = FindItem(slotInfo.LibItemId);
					if ( libItem != null )
					{
						name = libItem.Name;
						desc = libItem.Desc;
						size = libItem.Size;
						originFile = libItem.OrigSourceFile;
					}
					
					string[] args =
					{
						libItem != null ? HttpUtility.HtmlEncode(libItem.Id) : string.Empty,
						(slotIndex+1).ToString(),	
						HttpUtility.HtmlEncode(name),	
						HttpUtility.HtmlEncode(desc),
						size.ToString(),
						HttpUtility.HtmlEncode(originFile),
					};
					
					sb.AppendFormat("\t\t<Loop id=\"{0}\" slot=\"{1}\" name=\"{2}\" desc=\"{3}\" size=\"{4}\" originfile=\"{5}\" />", args );
					sb.AppendLine();
				}
				
				sb.AppendLine("\t</Loops>");				
				sb.AppendLine("</JMCSync>");
				
				XmlDocument xmlJMCSync = new XmlDocument();
				xmlJMCSync.LoadXml(sb.ToString());
				xmlJMCSync.Save(connectionInfo.JMCSyncFullPathFile);
				connectionInfo.SetJamManFullPath(connectionInfo.JMCSyncFullPathFile); // special case to ensure all flags set
				
				saveResult = true;
			}
			catch ( Exception x )
			{
				errorMessage = x.Message;
				saveResult = false;	
			}
			
			return saveResult;
		}
		
		public bool SwapUp (int libraryIndex)
		{
			// libraryIndex is zero based!
			if (libraryIndex <= 0 || libraryIndex > mLibraryItems.Count - 1)
			{
				return false;
			}

			JMCLibraryItem temp = mLibraryItems[libraryIndex-1];
			mLibraryItems[libraryIndex-1] = mLibraryItems[libraryIndex];
			mLibraryItems[libraryIndex] = temp;

			mLibraryItems[libraryIndex - 1].Index--;
			mLibraryItems[libraryIndex].Index++;

			return true;
		}

		public bool SwapDown (int libraryIndex)
		{
			// libraryIndex is zero based!
			if (libraryIndex < 0 || libraryIndex >= mLibraryItems.Count - 1)
			{
				return false;
			}

			JMCLibraryItem temp = mLibraryItems[libraryIndex+1];
			mLibraryItems[libraryIndex+1] = mLibraryItems[libraryIndex];
			mLibraryItems[libraryIndex] = temp;

			mLibraryItems[libraryIndex + 1].Index++;
			mLibraryItems[libraryIndex].Index--;

			return true;
		}
		
		public bool DeleteItem(JMCLibraryItem libItem)
		{
			if ( libItem == null )
			{
				return false;				
			}
			
			if ( mLibraryItems.Remove(libItem) == false )
			{
				return false;	
			}
			
			// Delete the associated WAV file, if any...
			string audioFile = this.GetAudioFileFullPath(libItem.Id, true );
			if ( string.IsNullOrWhiteSpace(audioFile) == false )
			{
				File.Delete(audioFile);				
			}
			
			return true;
		}
		
		public string GetAudioFileFullPath(string id, bool mustExist)
		{
			if ( string.IsNullOrWhiteSpace(id) || mIsLibraryLoaded == false)
			{
				return string.Empty;
			}
			
			string shortLibFileName = id + ".wav";
			string libWavFullFileName = System.IO.Path.Combine(this.mCurrentLibraryRootPath, shortLibFileName);
			
			if ( mustExist && File.Exists(libWavFullFileName) == false )
			{
				return string.Empty;					
			}
			
			return libWavFullFileName;
		}
		
		private void ResetToUnloadedLibrary()
		{
			// re-allocated primary buffers and values
			mLibraryItems = new List<JMCLibraryItem>();
			mLibraryInfo = new JMCLibraryInfo();
			mCurrentLibraryRootPath = string.Empty;
			mIsLibraryLoaded = false;	
		}
		
		public bool Load(string sourceFile)
		{
			// re-allocated primary buffers and values
			ResetToUnloadedLibrary();

			XmlDocument xdoc = new XmlDocument();

			try
			{
				xdoc.Load(sourceFile);
				
				XmlNodeList libraryInfo = xdoc.SelectNodes("//JMCLibrary/JMCLibraryInfo");
				
				// if there's data, read it...
				if ( libraryInfo != null && libraryInfo.Count == 1 )
				{
					mLibraryInfo.Version 			= GetSafeAttributeString(libraryInfo[0].Attributes["version"]);
					mLibraryInfo.Name 				= GetSafeAttributeString(libraryInfo[0].Attributes["name"]);
					mLibraryInfo.Author 			= GetSafeAttributeString(libraryInfo[0].Attributes["author"]);
					mLibraryInfo.Credits 			= GetSafeAttributeString(libraryInfo[0].Attributes["credits"]);
					mLibraryInfo.ConvertWAVToMono 	= GetSafeAttributeBool(libraryInfo[0].Attributes["convertWavToMono"]);
					mLibraryInfo.LibraryId 			= GetSafeAttributeString(libraryInfo[0].Attributes["id"]);
				
					// we must have this information!
					if ( string.IsNullOrWhiteSpace(mLibraryInfo.Version) ||
						 string.IsNullOrWhiteSpace(mLibraryInfo.Name) ||
						 string.IsNullOrWhiteSpace(mLibraryInfo.LibraryId) ) 
					{
						// can't load because missing basic information!
						throw new Exception();		
					}
				}
				else
				{
					// can't load because missing basic information!
					throw new Exception();		
				}
				
				XmlNodeList libraryItems = xdoc.SelectNodes("//JMCLibrary/JMCLibraryItems/JMCLibraryItem");
				if ( libraryItems != null && libraryItems.Count > 0 )
				{
					var libIndex = 0;
					foreach ( XmlNode libraryItem in libraryItems )
					{
						JMCLibraryItem libItem = new JMCLibraryItem();
						
						libItem.Id = GetSafeAttributeString(libraryItem.Attributes["id"]);
						libItem.OrigSourceFile = GetSafeAttributeString(libraryItem.Attributes["origsourcefile"]);
						libItem.Name = GetSafeAttributeString(libraryItem.Attributes["name"]);
						libItem.Desc = GetSafeAttributeString(libraryItem.Attributes["desc"]);
						libItem.Size = GetSafeAttributeLong(libraryItem.Attributes["size"]);
						libItem.TimeStamp = GetSafeAttributeDateTime(libraryItem.Attributes["timestamp"]);
						libItem.Index = libIndex + 1;

						string loopxml = GetSafeAttributeString(libraryItem.Attributes["loopxml"]);
						loopxml = HttpUtility.HtmlDecode(loopxml);
						
						libItem.LoopInfo = JamManLoopInfo.FromXML(loopxml);
						
						mLibraryItems.Add(libItem);
						
						libIndex++;
					}
				}
	
				// Library was successfully loaded
				mCurrentLibraryRootPath = System.IO.Path.GetDirectoryName(sourceFile);
				
				mIsLibraryLoaded = true;
			}
			catch
			{
				// Failed to load library... // FUTURE: may want to show error message!
				ResetToUnloadedLibrary();	
			}
			
			return mIsLibraryLoaded;
		}
		
		public bool AddFileToLibrary(JamManLoopInfo loopInfo, string waveFile, string name, string desc, ShowStatus showStatus, JMCSettings settings)
		{
			bool addResult = false;
			string errorMessage = string.Empty;
			
			try
			{
				if ( string.IsNullOrWhiteSpace(mCurrentLibraryRootPath) )
				{
					return false;	
				}
				
				JMCLibraryItem libraryItem = new JMCLibraryItem();

				libraryItem.LoopInfo = loopInfo;

				libraryItem.Id = Guid.NewGuid().ToString();
				libraryItem.Name = name;
				libraryItem.Desc = desc;
				libraryItem.OrigSourceFile = waveFile;
				libraryItem.TimeStamp = DateTime.Now;

				// Handle overrides of newly created library item:
				if ( settings != null )
				{
					if ( settings.DefaultNewLoopsToSinglePlayMode )
					{
						// user settings override the default loop mode for newly imported loops
						libraryItem.LoopInfo.LoopModeType = LoopMode.Single;
					}
				}
				
				FileInfo fi = new FileInfo(waveFile);
				libraryItem.Size = fi.Length;
				
				string shortLibFileName = libraryItem.Id + ".wav";
				string libWavFullFileName = System.IO.Path.Combine(mCurrentLibraryRootPath, shortLibFileName);

				// In version 2 it's up to user to make sure the
				// WAV file is converted to 16-bit 44.1 - we don't convert 
				// because the legacy WAV converter may actually be doing 
				// things wrong - we'll let the user find out if their file
				// works correctly when on the DigiTech device. But we won't muck with it anymore! -RB
				FileCopyAsync(waveFile, libWavFullFileName, showStatus, out errorMessage);

				// NOTE: see above.  No more converting for V2 for now! -RB
				//if ( NeedsConverting(waveFile))
				//{
				//	// FUTURE: might need to get back the type of conversion needed.
				//	// For now we only support one conversion type, mono, 16-bit 44.1
				//	WAVFileConvertAndCopyAsync(waveFile, libWavFullFileName, 16, false, showStatus);
				//}
				//else
				//{
				//	FileCopyAsync(waveFile, libWavFullFileName, showStatus, out errorMessage);
				//}
				
				if ( File.Exists(libWavFullFileName) )
				{
					mLibraryItems.Add(libraryItem);
					addResult = true;
				}
			}
			catch
			{
				addResult = false;
			}
			
			return addResult;
		}
		
		// TODO: too complex for commandline and may not be needed as Web app can potentially import multiple and call the single add version.
		//public bool AddFilesToLibrary(string[] waveFiles, ShowStatus showStatus, JMCSettings settings)
		//{
		//	bool addResult = false;
		//	try
		//	{
		//		foreach ( string wavFile in waveFiles )
		//		{
		//			FileInfo fi = new FileInfo (wavFile);
		//			string titleName = fi.Name.Replace(".wav", "");
					
		//			if ( AddFileToLibrary(wavFile, titleName, string.Empty, showStatus, settings) == false )
		//			{
		//				throw new Exception();	
		//			}
		//		}
				
		//		addResult = true;
		//	}
		//	catch
		//	{
		//		addResult = false;
		//	}
			
		//	return addResult;
		//}
		
		
		public bool SaveLibrary(string targetFile, bool mustBeLoaded)
		{
			if ( mustBeLoaded && mIsLibraryLoaded == false )
			{
				return false;	
			}
			
			bool saveResult = false;
			try
			{
				StringBuilder xml = new StringBuilder();
				
				xml.AppendLine("<JMCLibrary>");
				xml.Append(this.mLibraryInfo.ToXML());
				xml.AppendLine("\t<JMCLibraryItems>");
				
				foreach ( JMCLibraryItem libItem in mLibraryItems )
				{
					xml.AppendLine(libItem.ToXML());
				}
				
				xml.AppendLine("\t</JMCLibraryItems>");
				xml.AppendLine("</JMCLibrary>");
				
				XmlDocument xmlDoc = new XmlDocument();
				xmlDoc.LoadXml(xml.ToString());
				
				if ( File.Exists(targetFile) )
				{
					File.Delete(targetFile);	
				}
				
				xmlDoc.Save(targetFile);
				
				saveResult = true;
			}
			catch ( Exception x)
			{
				saveResult = false;
				var m = x.Message;
			}
			
			return saveResult;
		}

		static public string GetSafeAttributeString (XmlAttribute attribute)
		{
			if ( attribute == null ) return string.Empty;
			if ( string.IsNullOrWhiteSpace(attribute.Value) ) return string.Empty;
			
			return HttpUtility.HtmlDecode(attribute.Value);
		}
		
		static public int GetSafeAttributeInt (XmlAttribute attribute)
		{
			string s = GetSafeAttributeString(attribute);
			if ( string.IsNullOrWhiteSpace(s)) return 0;
			
			int i; 
			if ( int.TryParse(s, out i ) == false )
			{
				return 0;	
			}
			
			return i;
		}
		
		static public long GetSafeAttributeLong (XmlAttribute attribute)
		{
			string s = GetSafeAttributeString(attribute);
			if ( string.IsNullOrWhiteSpace(s)) return 0;
			
			long n; 
			if ( long.TryParse(s, out n ) == false )
			{
				return 0;	
			}
			
			return n;
		}
		
		static public double GetSafeAttributeDouble (XmlAttribute attribute)
		{
			string s = GetSafeAttributeString(attribute);
			if ( string.IsNullOrWhiteSpace(s)) return 0;
			
			double n; 
			if ( double.TryParse(s, out n ) == false )
			{
				return 0;	
			}
			
			return n;
		}
		
		static public bool GetSafeAttributeBool (XmlAttribute attribute)
		{
			string s = GetSafeAttributeString(attribute);
			if ( string.IsNullOrWhiteSpace(s)) return false;
			
			bool n; 
			if ( bool.TryParse(s, out n ) == false )
			{
				return false;	
			}
			
			return n;
		}
		
		static public DateTime GetSafeAttributeDateTime (XmlAttribute attribute)
		{
			string s = GetSafeAttributeString(attribute);
			if ( string.IsNullOrWhiteSpace(s)) return DateTime.MinValue;
			
			DateTime n; 
			if ( DateTime.TryParse(s, out n ) == false )
			{
				return DateTime.MinValue;	
			}
			
			return n;
		}
		
		static public Guid GetSafeAttributeGuid(XmlAttribute attribute)
		{
			string s = GetSafeAttributeString(attribute);
			if ( string.IsNullOrWhiteSpace(s)) return Guid.NewGuid();
			
			Guid n; 
			if ( Guid.TryParse(s, out n ) == false )
			{
				return Guid.NewGuid();	
			}
			
			return n;	
		}
		
		public bool GenerateLibraryReport(string targetFile)
		{
			bool saveResult = false;
			
			try
			{
				if ( File.Exists(targetFile) )
				{
					File.Delete(targetFile);	
				}
				
				StringBuilder sb = new StringBuilder();
				
				sb.AppendLine("<html>");
				sb.AppendLine("<head>");
				sb.AppendLine("<title>JMCLibrary Report</title>");
				sb.AppendLine("</head>");
				sb.AppendLine("<body>");
				
				sb.AppendLine("<h2>" + mLibraryInfo.Name + "</h2>");
				
				// FUTURE: more advanced? additional values?
				sb.AppendLine("<table width=\"100%\" border=\"1\">");
				
				// NOTE: this report is from memory,
				// so it should reflect the current state:
				
				sb.AppendLine("<tr align=\"left\"><th>Order</th><th>Name</th><th>Description</th><th>Loop</th><th>Modified</th></tr>");
				
				int itemIndex = 0;
				foreach ( JMCLibraryItem libItem in mLibraryItems )
				{
					itemIndex++;
					string libWavFile = Path.Combine(mCurrentLibraryRootPath, libItem.Id + ".wav" );
					string link = string.Format ("<a href=\"{0}\" title=\"{1}\" target=\"_blank\">{1}</a>", libWavFile, libItem.Name );
					sb.AppendFormat("<tr valign=\"top\"><td>{0}</td><td>{1}</td><td>{2}</td><td><textarea style=\"\">{3}</textarea></td><td>{4:yyyy-MM-dd hh:mm:ss}</td></tr>", itemIndex, link, libItem.Desc, libItem.LoopInfo.ToXML(), libItem.TimeStamp );
				}
				
				sb.AppendLine("</table>");
				
				sb.AppendLine ( "<h4><a href=\"https://www.birdwellmusic.com/contact\" target=\"_blank\">Contact for Support/Features</a></h4>"); 
				
				sb.AppendLine("</body>");
				
				sb.AppendLine("</html>");
				
				XmlDocument xHtmlDoc = new XmlDocument();
				xHtmlDoc.LoadXml(sb.ToString());
				xHtmlDoc.Save(targetFile);
				
				saveResult = File.Exists(targetFile);
			}
			catch
			{
				saveResult = false;
			}
			
			return saveResult;
		}
		
		public bool GenerateJamManReport(JMCLibrary library, JamManConnectInfo connectionInfo, string targetFile)
		{
			if ( connectionInfo == null )
			{
				return false;	
			}
	
			bool saveResult = false;
			
			try
			{
				if ( File.Exists(targetFile) )
				{
					File.Delete(targetFile);	
				}
				
				StringBuilder sb = new StringBuilder();
				
				sb.AppendLine("<html>");
				sb.AppendLine("<head>");
				sb.AppendLine("<title>JamMan Slot Report</title>");
				sb.AppendLine("</head>");
				sb.AppendLine("<body>");
				
				sb.AppendLine("<h2>JamMan Location: " + connectionInfo.JamManFullPath + "</h2>");
				
				// FUTURE: more advanced? additional values?
				sb.AppendLine("<table width=\"100%\" border=\"1\">");
				
				// NOTE: this report is from memory,
				// so it should reflect the current state:
				
				sb.AppendLine("<tr align=\"left\"><th>Slot</th><th>Name</th><th>Description</th><th>Loop</th><th>Modified</th></tr>");
				
				for ( int slotIndex= 0; slotIndex < connectionInfo.SlotCount; slotIndex++ )
				{
					JamManSlotInfo slotInfo = connectionInfo.SlotFromIndex(slotIndex);
					
					string loopWavFile = string.Empty;
					string loopXmlFile = string.Empty;
					string link = string.Empty;
					string loopLink = "---";
					string desc = "";
					DateTime timeStamp = DateTime.MinValue;
					
					if ( slotInfo.IsEmpty )
					{
						link = "[EMPTY]";
					}
					else
					{
						// We already have the slotInfo but we need the library item to get the description
						// and also this ensures we really have the correct data!
						JMCLibraryItem libItem = library.FindItem(slotInfo.LibItemId);
						if ( libItem == null )
						{
							libItem = new JMCLibraryItem(); // default to something!	
						}
						
						string slotFolder = string.Format ("LOOP{0:00}", slotIndex+1);
						loopWavFile = Path.Combine(connectionInfo.JamManFullPath, slotFolder, "LOOP.WAV" );
						loopXmlFile = Path.Combine(connectionInfo.JamManFullPath, slotFolder, "LOOP.XML" );
						desc = libItem.Desc;
						timeStamp = libItem.TimeStamp;
						link = string.Format ("<a href=\"{0}\" title=\"{1}\" target=\"_blank\">{1}</a>", loopWavFile, libItem.Name );		
						loopLink = string.Format ("<a href=\"{0}\" title=\"{1}\" target=\"_blank\">Details</a>", loopXmlFile, libItem.Name );		
					}
				
					sb.AppendFormat("<tr valign=\"top\"><td>{0}</td><td>{1}</td><td>{2}</td><td>{3}</td><td>{4:yyyy-MM-dd hh:mm:ss}</td></tr>", slotIndex+1, link, desc, loopLink, timeStamp);
				}
				
				sb.AppendLine("</table>");
				
				sb.AppendLine ( "<h4><a href=\"https://www.birdwellmusic.com/contact\" target=\"_blank\">Contact for Support/Features</a></h4>"); 
				
				sb.AppendLine("</body>");
				
				sb.AppendLine("</html>");
				
				XmlDocument xHtmlDoc = new XmlDocument();
				xHtmlDoc.LoadXml(sb.ToString());
				xHtmlDoc.Save(targetFile);
				
				saveResult = File.Exists(targetFile);
			}
			catch
			{
				saveResult = false;	
			}
			
			return saveResult;
		}
		
		public static void FileCopyAsync(string sourceFile, string destFile, ShowStatus showStatus, out string errorMessage)
		{
			errorMessage = string.Empty;
			FileCopy(sourceFile, destFile, showStatus, out errorMessage);
		}

		public static void FileCopy(string sourceFile, string destFile, ShowStatus showStatus, out string errorMessage)
        { 
			errorMessage = string.Empty;
			// Adapted from:
			// http://stackoverflow.com/questions/6044629/file-copy-with-progress-bar
			
			try
			{
				showStatus?.Invoke("Please wait: copying file " + sourceFile + " to " + destFile, .1);

				byte[] buffer = new byte[1024*1024]; // 1MB buffer

	            using (FileStream source = new FileStream(sourceFile, FileMode.Open, FileAccess.Read))
	            {
	                long fileLength = source.Length;
	                using (FileStream dest = new FileStream(destFile, FileMode.CreateNew, FileAccess.Write))
	                {
	                    long bytesCopied = 0;
	                    int currentBlockSize = 0;
	
	                    while ((currentBlockSize = source.Read(buffer, 0, buffer.Length)) > 0)
	                    {
							bytesCopied += currentBlockSize;
	                        double percentDone = bytesCopied / (double)fileLength;

							showStatus?.Invoke("Copying file " + sourceFile + " to " + destFile, percentDone);	
							
	                        dest.Write(buffer, 0, currentBlockSize);
	                    }
	                }
	            }	
				
				
			}
			catch ( Exception x )
			{
				// Capture this so it can be bubbled!
				errorMessage = x.Message;
				
				// Catch it...will have to be handled up the stack
				// by virtue of the fact that the expected destination
				// file does not exist!
				showStatus?.Invoke("FILE COPY ERROR: " + x.Message, 1 );
				
				return;
			}
			
			showStatus?.Invoke("Ready.", 0F );
        }
		
//        public static void FileCopy(string sourceFile, string destFile, ShowStatus showStatus)
//        { 
//			try
//			{
//				File.Copy (sourceFile, destFile);
//			
//				if ( showStatus != null )
//				{
//					showStatus("Ready.", 0F );
//				}
//			}
//			catch
//			{
//				// eat it...
//			}
//        }

//        public static void FileCopyAsyncCompleted(IAsyncResult result)
//        {
//            // FUTURE: Code to be run after FileCopyAsync is done
//        }
		
		public static void WAVFileConvertAndCopyAsync(string sourceFile, string destFile, short bitsPerSample, bool isStereo, ShowStatus showStatus)
        {
            if ( showStatus != null )
			{
				showStatus("Please wait: copying file " + sourceFile + " to " + destFile, .5 );	
			}
			
			// FUTURE: see FileCopyAsync...
			//WAVFileConvertAndCopyDelegate del = new WAVFileConvertAndCopyDelegate(WAVFileConvertAndCopy);
			//IAsyncResult result = del.BeginInvoke(sourceFile, destFile, bitsPerSample, isStereo, showStatus, WAVFileConvertAndCopyAsyncCompleted, null);
			
			// A psuedo async call since showStatus will flush message...
			WAVFileConvertAndCopy(sourceFile, destFile, bitsPerSample, isStereo, showStatus);
		}
		
		public static void WAVFileConvertAndCopy(string sourceFile, string destFile, short bitsPerSample, bool isStereo, ShowStatus showStatus)
		{
			try
			{
				WAVFile.CopyAndConvert(sourceFile,destFile, bitsPerSample, isStereo, 1.0, showStatus );
	
				if ( showStatus != null )
				{
					showStatus("Ready.", 0F );
				}
			}
			catch
			{
				// eat it...	
			}
		}

//        public static void WAVFileConvertAndCopyAsyncCompleted(IAsyncResult result)
//        {
//            // FUTURE: Code to be run after WAVFileConvertAndCopyAsync is done
//        }
	}
}

