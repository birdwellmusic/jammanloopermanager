# JamMan Classic Looper Manager 
- Author: Rob Birdwell
- https://www.birdwellmusic.com

## .NET Core Dependency ##
- This software requires **.NET Core 7** or higher to run on your computer
- https://learn.microsoft.com/en-us/dotnet/core/install/how-to-detect-installed-versions?pivots=os-windows

## Key Concepts ##
- This librarian runs as a localhost Web App on your computer.
- This is safe and portable - it can run on multiple operating systems: Windows, Linus, Mac
- There's currently no "simple" install - but it's not too hard. I'll refine these steps and hopefully simplify in time!

## Installation of the Web App
- Go to this link: **https://bitbucket.org/birdwellmusic/jammanloopermanager/downloads/**
- Click the **"Download repository"** link - once downloaded, **unzip this file on your local computer**;
- Copy the your **\JamManWeb\Distributions\portable** folder - this should work for Windows, Linux, and Mac
- Paste and rename the folder you copied to a new location on your computer, e.g., **C:\MyJamMan**
- Run **JamManWeb.exe** which will start the web server (this will open a console/terminal window - leave that window open while running this tool)
- Once the web server is running, paste this url in your browser: **http://localhost:5000**
- That's it! Start importing loops and doing stuff!
- Note that your **JAMMAN** folder is **virtual** (This tool does **NOT** write to your device's **JAMMAN** card directly). Once you've saved/published to it you copy the files in that folder to your actual **JAMMAN** card folder.
- For example, if your installation folder is this, **C:\MyJamMan** then after running the web site locally your virtual card will be **C:\MyJamMan\JAMMAN**
- Once you have your slots defined in the web tool the way you want, you'll need to manually copy the **C:\MyJamMan\JAMMAN** folder to your device card's **JAMMAN** folder, replacing all those files.

## Commandline Version
- It's separate from the web app; won't share any of the same libary/jamman stuff
- Open up a command line in the *\jammanloopermanager\JamManCommand\bin\Debug\[NET Core Version]* directory
- jm.exe is the commandline tool
- Example: jm -initAll
- See zzzTest.bat - it has some examples and was a proof of concept but it's what the web app is built on.
- See *\jammanloopermanager\JamManCommand\Program.cs* for all the possible commands.

## Dev Tools Required:
- Visual Studio
- .NET Core & Blazor
- BitBucket Repo: https://bitbucket.org/birdwellmusic/jammanloopermanager

## Known Issues & Areas For Improvement for this Open Source Project:
* Some minor issues - see TODO: comments in code
* Documentation! This might be it! ;)

## Comments:  
* Written originally for my (Rob Birdwell) personal use.  The absence of a librarian tool was a pain so I gave it a go.
* This version attempts to maintain the spirit of portablity using .NET, C#, Blazor and WebAssembly in order to make a rich web app that can be run on any client! 
* Naturally, due to my desire to spend time writing music, I punted on some "hard" features, but functionally, it's a working librarian. 
* Questions? https://www.birdwellmusic.com/contact



