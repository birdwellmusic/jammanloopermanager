$(document).ready(function () {

	// NOTE: not used by the Blazor/Web application.
	// this script is only here for reference and is used for
	// the command-line tool that generates the static HTML.  

	var commandHelp = "---- COMMON COMMANDS ----\n";
	commandHelp += "jm -initAll\n";
	$(".commandBox").text(commandHelp);

	var selectedSlotLibItemId = "";
	var selectedSlotNum = "";
	var loopmode = "";
	var tempo = "";
	var timesig = "";
	var rhythm = "";
	var stopmode = "";
	var libItemId = "";
	var libItemName = "";
	var libItemDesc = "";
	var libItemIndex = "";

	$(".slotLibItemRadio").click(function () {

		// when a slot's radio is clicked we will send and event to
		// click the corresponding library item so it displays its properties.
		if ($(this).is(':checked')) {
			selectedSlotLibItemId = $(this).data('slotlibitemid');
			selectedSlotNum = $(this).data('slotnum');
			if (selectedSlotLibItemId !== "") {
				$('.libItemRadio[data-libitemid=' + selectedSlotLibItemId + ']').click();
			}
			else {
				// an empty slot is checked - append the commands possible:
				if (libItemId !== "" && selectedSlotNum !== "") {
					var appendedCommandHelp = "// Assign " + libItemName + " to EMPTY slot #" + selectedSlotNum + "\n";
					appendedCommandHelp += "jm -jammanAssignSlot " + libItemId + " " + selectedSlotNum + "\n";

					$(".commandBox").text(appendedCommandHelp);
				}
			}
		}
	});


	$(".libItemRadio").click(function () {

		if ($(this).is(':checked')) {
			loopmode = $(this).data('loopmode');
			tempo = $(this).data('tempo');
			timesig = $(this).data('timesig');
			rhythm = $(this).data('rhythm');
			stopmode = $(this).data('stopmode');

			// additional data, not sure how to display yet or if needed...
			libItemId = $(this).data('libitemid');
			libItemName = $(this).data('libitemname');
			libItemDesc = $(this).data('libitemdesc');
			libitemIndex = $(this).data('libitemindex');
		}

		// display these in the loop info area...

		$("#libSelectedLoopName").text(libItemName);
		$("#libItemId").text(libItemId);

		$("#libLoopInfoLoopMode").text(loopmode);
		$("#libLoopInfoTempo").text(TempoToBPM(tempo));
		$("#libLoopInfoTimeSig").text(timesig);
		$("#libLoopInfoRhythmType").text(rhythm);
		$("#libLoopInfoStopMode").text(stopmode);

		///////////////////////////////////////////////////////////////////////////////////////////////////////////////
		// NOTE: the initial static index.html was a proof of concept to see if I could set the list of possible commands with data...we can
		// and it only got much much better with Blazor!!
		// selectedSlotLibItemId
		// selectedSlotNum
		// loopmode
		// tempo
		// timesig 
		// rhythm
		// stopmode
		// libItemId
		// libItemName
		// libItemDesc
		// libItemIndex

		commandHelp = "---- COMMON COMMANDS ----\n";
		commandHelp += "jm -initAll\n";

		if (libItemId !== "") {
			commandHelp += "---- LIBRARY COMMANDS ----\n";
			commandHelp += "// Remove " + libItemName + " with id of " + libItemId + "\n";
			commandHelp += "jm -libraryRemove " + libItemId + "\n";
			commandHelp += "// Archive a copy of " + libItemName + " (this does NOT remove item from library)\n";
			commandHelp += "jm -libraryArchive " + libItemId + "\n";
			commandHelp += "jm -libraryReport\n";

			commandHelp += "// Update this item - see tips for valid values\n";
			commandHelp += "jm -libraryUpdate " + libItemId + " \"" + libItemName + "\" " + "\"" + libItemDesc + "\" " + loopmode + " " + TempoToBPM(tempo) + " " + timesig + " " + rhythm + " " + stopmode + "\n";

			// NOTE: more commands... this is only for the static index.html cache file. It was a proof of concept
			// and probably no need to explore this more as the Blazor app has mostly made this obsolete, but it's
			// here for use if anyone ever really wanted to do it all from the commandline with minimal UI other
			// than an index.html cache!
		}

		if (selectedSlotNum !== "") {
			commandHelp += "---- JAMMAN COMMANDS ----\n";

			commandHelp += "jm -jammanClearAllSlots\n";
			commandHelp += "jm -jammanSaveAll\n";
			commandHelp += "jm -jammanSyncToLibrary\n";
			commandHelp += "jm -jammanReport\n";


			if (selectedSlotLibItemId !== "") {
				commandHelp += "jm -jammanClearSlot " + selectedSlotNum + "\n";
			}

			if (selectedSlotLibItemId !== "" && libItemId !== "") {
				commandHelp += "// Assign " + libItemName + " to slot " + selectedSlotNum + "\n";
				commandHelp += "jm -jammanAssignSlot " + libItemId + " " + selectedSlotNum + "\n";
			}

			// NOTE: more commands...? Probably not for this static use as the concept was proved
			// and Blazor does it all without any jQuery...
		}

		// update the command box...
		$(".commandBox").text(commandHelp);
		///////////////////////////////////////////////////////////////////////////////////////////////////////////////


	});

	function TempoToBPM(tempoSamplesPerSecond) {
		if (tempoSamplesPerSecond <= 0) {
			return 120; // don't default to zero!
		}

		return Math.round(2646000 / tempoSamplesPerSecond);
	}

});