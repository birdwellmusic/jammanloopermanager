﻿using System.IO;
using System.Linq;
using System.Threading.Tasks;
using JamManClassicLib;

namespace JamManWeb.Data
{
    public class JamManDataService
    {
        public Task<JamManJSONData> GetJamManJSONDataAsync()
        {
            return Task.FromResult(JMCommands.GetJamManJSONData());
        }

        public void InitAll(JMNotifier notify)
        {
            JMCommands.InitAll(notify);
        }

        public Task<byte[]> GetAudioWavAsync(string libId, JamManJSONData jammanData)
        {
            // don't return null if the file doesn't exist - let the consumer handle that part!
            var emptyBytes = new byte[0];

            if (jammanData == null)
            {
                return Task.FromResult(emptyBytes);
            }

            // find the library item...

            var libItem = jammanData.Library.LibraryItems.FirstOrDefault(x => x.Id == libId);

            if (libItem == null)
            {
                return Task.FromResult(emptyBytes);
            }

            var librarySrcWavFile = Path.Combine(jammanData.Library.CurrentLibraryRootPath, libItem.Id + ".wav");

            if (File.Exists(librarySrcWavFile) == false)
            {
                return Task.FromResult(emptyBytes);
            }

            var dataBytes = File.ReadAllBytes(librarySrcWavFile);

            if (dataBytes == null || dataBytes.Length == 0)
            {
                return Task.FromResult(emptyBytes);
            }

            return Task.FromResult(dataBytes);
        }
    }
}
