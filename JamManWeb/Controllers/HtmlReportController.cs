﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using JamManClassicLib;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace JamManWeb.Controllers
{
    [Route("api/HtmlReport")]
    [ApiController]
    public class HtmlReportController : ControllerBase
    {
        [HttpGet, Route("{name}")]
        public HttpResponseMessage Get(string name)
        {
            var html = string.Empty;
            var fileName = string.Empty;

            if ( name.ToLower() == "library")
            {
                html = JMCommands.GetLibraryReportHtml();
                fileName = "JMLibraryReport.html";
            }
            else if ( name.ToLower() == "jamman")
            {
                html = JMCommands.GetJamManReportHtml();
                fileName = "JamManReport.html";
            }
            else
            {
                return null;
            }

            var buffer = Encoding.UTF8.GetBytes(html);
            var stream = new MemoryStream(buffer);

            var result = new HttpResponseMessage(HttpStatusCode.OK)
            {
                Content = new ByteArrayContent(stream.ToArray())
            };
            
            result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment")
            {
                FileName = fileName
            };

            result.Content.Headers.ContentType = new MediaTypeHeaderValue("text/html");

            // TODO: download is not working...currently user has to copy the report url/file and fetch...
            return result;

            //var response = this.HttpContext.Response;

            //response.Headers.Add("Content-Type", "text/html");
            //response.Headers.Add("Content-Length", buffer.Length.ToString());
            //response.Headers.Add("Content-Disposition", "attachment; filename=" + fileName);

            //return File(stream, "text/html", fileName);
        }

        //[HttpGet]
        //public HttpResponseMessage Generate()
        //{
        //    var stream = new MemoryStream();
        //    // processing the stream.

        //    var result = new HttpResponseMessage(HttpStatusCode.OK)
        //    {
        //        Content = new ByteArrayContent(stream.ToArray())
        //    };
        //    result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment")
        //        {
        //            FileName = "CertificationCard.pdf"
        //        };

        //    result.Content.Headers.ContentType =
        //        new MediaTypeHeaderValue("application/octet-stream");

        //    return result;
        //}
    }
}