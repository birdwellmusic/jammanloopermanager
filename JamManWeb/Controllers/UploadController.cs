﻿using JamManClassicLib;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace JamManWeb.Controllers
{
    public class UploadController : Controller 
    {
        private readonly IWebHostEnvironment environment;
        public UploadController(IWebHostEnvironment environment)
        {
            this.environment = environment;
        }

        [HttpPost("upload/single")]
        public IActionResult SingleAsync(IFormFile file)
        {
            var statusCode = 200;
            var statusMessage = $"{file.FileName} was uploaded to the library"; // being optimistic!

            try
            {
                //var fullpathFileName = Path.Combine(environment.ContentRootPath, "JMUploads", file.FileName);
                var fullpathFileName = Path.Combine(JMCommands.GetAppPath(), "JMUploads", file.FileName);

                if (System.IO.File.Exists(fullpathFileName))
                {
                    System.IO.File.Delete(fullpathFileName);
                }

                FileStream fileStream = null;
                Stream uploadFileStream = null;

                try
                {
                    uploadFileStream = file.OpenReadStream();
                    fileStream = new FileStream(fullpathFileName, FileMode.Create, FileAccess.Write);

                    uploadFileStream.CopyTo(fileStream);
                }
                catch (Exception x)
                {
                    statusCode = 500;
                    statusMessage = x.Message;
                }
                finally
                {
                    fileStream?.Close();
                    uploadFileStream?.Close();
                }

                return StatusCode(200);
            }
            catch (Exception ex)
            {
                statusCode = 500;
                statusMessage = ex.Message;
            }

            return StatusCode(statusCode, statusMessage);
        }
    }
}